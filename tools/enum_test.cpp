#include "com.example.domain.Constants.h"
#include <iostream>
#include <cassert>
#include <ya/utils/string.h>

using com::example::domain::Constants;
using ya::utils::from_string;

int main(int argc, char* argv[])
{

    for (auto c : enum_list(Constants{})) {
        std::cout << c << std::endl;
        assert(c == from_string<Constants>(to_string(c)));
    }

    return 0;
} /* end main() */
