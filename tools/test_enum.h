/**
 * GENERATED CODE FROM "test_enum.enum" FILE DO NOT MODIFY
 */
#pragma once
#include <string>
#include <iostream>
#include <array>

namespace ya {
namespace utils {

enum class queue_code
{
    queue_ok,
    queue_is_empty,
    queue_is_full,
    queue_is_closed
};

const char* to_cstring(queue_code value);
std::string to_string(queue_code value);
std::ostream& operator<<(std::ostream& out, queue_code value);
std::istream& operator>>(std::istream& out, queue_code& value);
std::array<queue_code, 4> const& enum_list(queue_code dummy);

} /* namespace utils */
} /* namespace ya */
