#!/usr/bin/env python3

class CppGenConfig:
    
    def indentSize(self):
        return 4

    def indentNamespace(self):
        return False
    
    def breakBeforeOpeningCurlyBracket(self):
        return False

    def breakAfterOpeningCurlyBracket(self):
        return True

    def breakBeforeClosingCurlyBracket(self):
        return True

    def breakAfterClosingCurlyBracket(self):
        return False
    pass

class BlockName:
    def __init__(self, name, blockname=''):
        self.name = name
        self.blockname = blockname
        pass

    pass

class CppGen:
    def __init__(self, output, config = None):
        self.__indent = 0
        self.__curlyBrackets = 0
        if config is not None:
            self.__config = config
        else:
            self.__config = CppGenConfig()

        self.__output = output
        self.__names = []
        self.__newLines = 0

    def pushName(self, name, blockname):
        self.__names.append(BlockName(name, blockname))
        pass

    def popName(self, blockname=''):
        l = len(self.__names)
        if l > 0:
            bn = self.__names.pop()
            if bn.blockname == blockname:
                return bn.name
            else:
                # TODO raise
                return ''
        else:
            # TODO raise
            pass

        pass

    def config(self):
        return self.__config

    def print(self, arg):
        self.__output.write(arg)
        argLen = len(arg)
        if argLen > 0:
            newLines = 0
            for i in range(1, argLen+1):
                if arg[-i] == '\n':
                    newLines = newLines + 1
                else:
                    break;

            if i == argLen:
                self.__newLines += newLines
            else:
                self.__newLines = newLines

    def incIndent(self, indent):
        self.__indent = self.__indent + indent

    def decIndent(self, indent=1):
        self.__indent = self.__indent - indent

    def printIndent(self, mod=0):
        self.print(' ' * self.config().indentSize()*(self.__indent+mod))

    def breakLine(self, n=1):
        for i in range(self.__newLines, n):
            self.print('\n')

    def openCurlyBracket(self, indent=1):
        if self.config().breakBeforeOpeningCurlyBracket():
            self.breakLine()
            self.printIndent()
        else:
            self.print(' ')

        self.print('{')
        self.incIndent(indent)
        self.__curlyBrackets = self.__curlyBrackets + 1

        if self.config().breakAfterOpeningCurlyBracket():
            self.breakLine()

    def closeCurlyBracket(self, indent=1):
        self.decIndent(indent)
        if self.config().breakBeforeClosingCurlyBracket():
            self.breakLine()
            self.printIndent()
        else:
            self.print(' ')

        self.print('}')
        self.__curlyBrackets = self.__curlyBrackets - 1

    def openNamespace(self, ns):
        self.printIndent()
        self.print('namespace ' + ns);
        self.pushName(ns, 'namespace')
        if self.config().indentNamespace():
            self.openCurlyBracket();
        else:
            self.openCurlyBracket(0);

    def closeNamespace(self, ns=None):
        if self.config().indentNamespace():
            self.closeCurlyBracket();
        else:
            self.closeCurlyBracket(0);
        ns = self.popName('namespace')
        self.print(' /* namespace ' + ns + ' */');

    def openEnum(self, name, is_class=True):
        pass

    def closeEnum(self):
        pass

    def openClass(self, name, pub_parents=[], pro_parents=[], pri_parents=[]):
        
        pub = ['public ' + n for n in pub_parents]
        pro = ['protected ' + n for n in pro_parents]
        pri = ['private ' + n for n in pri_parents]

        self.breakLine(2)
        self.printIndent()
        self.print("class " + name)
        self.pushName(name, 'class')

        if len(pub_parents) > 0 or len(pro_parents) > 0 or len(pri_parents) > 0:
            first=True
            for parent in pub + pro + pri:
                self.breakLine();
                self.printIndent(1);
                if first:
                    self.print(': ')
                    first = False
                else:
                    self.print(', ')

                self.print(parent)
        self.openCurlyBracket(2);

    def closeClass(self):
        self.closeCurlyBracket(2);
        self.print('; /* ' + self.popName('class') + ' */')
        self.breakLine(2)

    def publicDeclarations(self):
        self.printIndent(-1)
        self.print('public:')
        self.breakLine()

    def protectedDeclarations(self):
        self.printIndent(-1)
        self.print('protected:')
        self.breakLine()

    def openIf(self, cond):
        self.printIndent()
        self.print("if ")
        self.print("(")
        self.print(cond)
        self.print(")")
        self.openCurlyBracket()
        pass

    def openElseIf(self, cond):
        self.closeCurlyBracket()
        
        self.print(' else if ')
        self.print("(")
        self.print(cond)
        self.print(")")
        self.openCurlyBracket()
        pass

    def openElse(self):
        self.closeCurlyBracket()
        self.print(' else')
        self.openCurlyBracket()
        pass

    def closeIf(self):
        self.closeCurlyBracket()
        pass

    def openFunction(self, ret, name, params):
        self.breakLine(2)
        self.printIndent()
        self.print(ret + ' ' + name + '('+ ', '.join(params) + ')')
        self.openCurlyBracket()
        pass

    def closeFunction(self):
        self.closeCurlyBracket()
        self.breakLine(2)
        pass

    def privateDeclarations(self):
        self.printIndent(-1)
        self.print('private:\n')
        pass
    pass

if __name__ == '__main__':
    with open("test.cpp", "w") as cpp:
        gen = CppGen(cpp, CppGenConfig())

        gen.openNamespace("test")
        gen.openNamespace("test2")
        gen.openNamespace("test3")
        gen.openClass("Class", ["PublicParent"], ["ProtectedParent"], ['PrivateParent'])
        gen.publicDeclarations()
        gen.openClass("InnerClass")
        gen.closeClass()
        gen.openFunction('void', 'foo',['int a', 'int b', 'std::string const& s'])
        gen.openIf('a < 3')
        gen.openElseIf('a > 5')
        gen.openElse()
        gen.closeIf()
        gen.closeFunction()
        gen.closeClass()
        gen.closeNamespace()
        gen.closeNamespace()
        gen.closeNamespace()
        gen.breakLine()
    

