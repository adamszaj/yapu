/**
 * GENERATED CODE FROM "test_enum.enum" FILE DO NOT MODIFY
 */
#include "test_enum.h"
#include <algorithm>

namespace ya {
namespace utils {

namespace {
struct queue_code_cmp
{
    bool operator()(queue_code lhs, std::string const& rhs)
    {
        return to_cstring(lhs) < rhs;
    }
    bool operator()(std::string const& lhs, queue_code rhs)
    {
        return lhs < to_cstring(rhs);
    }
};
const char* const names[] = {"queue_ok", "queue_is_empty", "queue_is_full", "queue_is_closed", nullptr};
}

std::array<queue_code, 4> const& enum_list(queue_code dummy)
{
    static const std::array<queue_code, 4> __list{
            queue_code::queue_ok, queue_code::queue_is_empty, queue_code::queue_is_full, queue_code::queue_is_closed};
    return __list;
}

static std::array<queue_code, 4> const& sorted_enum_list(queue_code dummy)
{
    static const std::array<queue_code, 4> __list{
            queue_code::queue_is_closed, queue_code::queue_is_empty, queue_code::queue_is_full, queue_code::queue_ok};
    return __list;
}

const char* to_cstring(queue_code value)
{
    return names[static_cast<unsigned>(value)];
}

std::string to_string(queue_code value)
{
    return to_cstring(value);
}

std::ostream& operator<<(std::ostream& out, queue_code value)
{
    return out << to_cstring(value);
}

std::istream& operator>>(std::istream& in, queue_code& value)
{
    std::string s;
    if (in >> s) {
        queue_code_cmp cmp;
        auto const& sorted = sorted_enum_list(queue_code{});
        auto it = std::lower_bound(sorted.begin(), sorted.end(), s, cmp);
        if ((it != sorted.end()) && (to_cstring(*it) == s)) {
            value = *it;
            return in;
        }
    }
    in.clear(std::ios::failbit);
    return in;
}

} /* namespace utils */
} /* namespace ya */
