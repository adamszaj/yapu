/**
 * GENERATED CODE FROM "com.example.domain.Constants.enum" FILE DO NOT MODIFY
 */
#include "com.example.domain.Constants.h"
#include <algorithm>

namespace com {
namespace example {
namespace domain {

namespace {
struct Constants_cmp
{
    bool operator()(Constants lhs, std::string const& rhs)
    {
        return to_cstring(lhs) < rhs;
    }
    bool operator()(std::string const& lhs, Constants rhs)
    {
        return lhs < to_cstring(rhs);
    }
};
const char* const names[] = {"com.example.domain.ala",   "com.example.domain.bela",  "com.example.domain.cela",
                             "com.example.domain.dela",  "com.example.domain.ela",   "com.example.domain.a.f.x",
                             "com.example.domain.a.f.y", "com.example.domain.a.f.z", "com.example.domain.a.g.x",
                             "com.example.domain.a.g.y", "com.example.domain.a.g.z", "com.example.domain.a.h.x",
                             "com.example.domain.a.h.y", "com.example.domain.a.h.z", "com.example.domain.b.f.x",
                             "com.example.domain.b.f.y", "com.example.domain.b.f.z", "com.example.domain.b.g.x",
                             "com.example.domain.b.g.y", "com.example.domain.b.g.z", "com.example.domain.b.h.x",
                             "com.example.domain.b.h.y", "com.example.domain.b.h.z", "com.example.domain.c.f.x",
                             "com.example.domain.c.f.y", "com.example.domain.c.f.z", "com.example.domain.c.g.x",
                             "com.example.domain.c.g.y", "com.example.domain.c.g.z", "com.example.domain.c.h.x",
                             "com.example.domain.c.h.y", "com.example.domain.c.h.z", nullptr};
}

std::array<Constants, 32> const& enum_list(Constants dummy)
{
    static const std::array<Constants, 32> __list{
            com_example_domain_ala,   com_example_domain_bela,  com_example_domain_cela,  com_example_domain_dela,
            com_example_domain_ela,   com_example_domain_a_f_x, com_example_domain_a_f_y, com_example_domain_a_f_z,
            com_example_domain_a_g_x, com_example_domain_a_g_y, com_example_domain_a_g_z, com_example_domain_a_h_x,
            com_example_domain_a_h_y, com_example_domain_a_h_z, com_example_domain_b_f_x, com_example_domain_b_f_y,
            com_example_domain_b_f_z, com_example_domain_b_g_x, com_example_domain_b_g_y, com_example_domain_b_g_z,
            com_example_domain_b_h_x, com_example_domain_b_h_y, com_example_domain_b_h_z, com_example_domain_c_f_x,
            com_example_domain_c_f_y, com_example_domain_c_f_z, com_example_domain_c_g_x, com_example_domain_c_g_y,
            com_example_domain_c_g_z, com_example_domain_c_h_x, com_example_domain_c_h_y, com_example_domain_c_h_z};
    return __list;
}

static std::array<Constants, 32> const& sorted_enum_list(Constants dummy)
{
    static const std::array<Constants, 32> __list{
            com_example_domain_a_f_x, com_example_domain_a_f_y, com_example_domain_a_f_z, com_example_domain_a_g_x,
            com_example_domain_a_g_y, com_example_domain_a_g_z, com_example_domain_a_h_x, com_example_domain_a_h_y,
            com_example_domain_a_h_z, com_example_domain_ala,   com_example_domain_b_f_x, com_example_domain_b_f_y,
            com_example_domain_b_f_z, com_example_domain_b_g_x, com_example_domain_b_g_y, com_example_domain_b_g_z,
            com_example_domain_b_h_x, com_example_domain_b_h_y, com_example_domain_b_h_z, com_example_domain_bela,
            com_example_domain_c_f_x, com_example_domain_c_f_y, com_example_domain_c_f_z, com_example_domain_c_g_x,
            com_example_domain_c_g_y, com_example_domain_c_g_z, com_example_domain_c_h_x, com_example_domain_c_h_y,
            com_example_domain_c_h_z, com_example_domain_cela,  com_example_domain_dela,  com_example_domain_ela};
    return __list;
}

const char* to_cstring(Constants value)
{
    return names[static_cast<unsigned>(value)];
}

std::string to_string(Constants value)
{
    return to_cstring(value);
}

std::ostream& operator<<(std::ostream& out, Constants value)
{
    return out << to_cstring(value);
}

std::istream& operator>>(std::istream& in, Constants& value)
{
    std::string s;
    if (in >> s) {
        Constants_cmp cmp;
        auto const& sorted = sorted_enum_list(Constants{});
        auto it = std::lower_bound(sorted.begin(), sorted.end(), s, cmp);
        if ((it != sorted.end()) && (to_cstring(*it) == s)) {
            value = *it;
            return in;
        }
    }
    in.clear(std::ios::failbit);
    return in;
}

} /* namespace domain */
} /* namespace example */
} /* namespace com */
