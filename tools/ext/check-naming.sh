#!/bin/bash

rm -fr build-cncc
mkdir -p build-cncc
pushd build-cncc
OPTIONS="-DCMAKE_C_COMPILER=/usr/bin/clang-3.8 -DCMAKE_CXX_COMPILER=/usr/bin/clang++-3.8 -DCMAKE_USER_MAKE_RULES_OVERRIDE=cmake/clang.cmake -D_CMAKE_TOOLCHAIN_PREFIX=llvm- -D_CMAKE_TOOLCHAIN_SUFFIX=-3.8"
    cmake .. ${OPTIONS} -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
    popd
FILES="`git ls-files '*.h'` `git ls-files '*.cpp'`"
cncc --dbdir=build-cncc --style=tools/ext/cncc.style ${FILES}

