/**
 * GENERATED CODE FROM "com.example.domain.Constants.enum" FILE DO NOT MODIFY
 */
#pragma once
#include <string>
#include <iostream>
#include <array>

namespace com {
namespace example {
namespace domain {

enum Constants
{
    com_example_domain_ala,
    com_example_domain_bela,
    com_example_domain_cela,
    com_example_domain_dela,
    com_example_domain_ela,
    com_example_domain_a_f_x,
    com_example_domain_a_f_y,
    com_example_domain_a_f_z,
    com_example_domain_a_g_x,
    com_example_domain_a_g_y,
    com_example_domain_a_g_z,
    com_example_domain_a_h_x,
    com_example_domain_a_h_y,
    com_example_domain_a_h_z,
    com_example_domain_b_f_x,
    com_example_domain_b_f_y,
    com_example_domain_b_f_z,
    com_example_domain_b_g_x,
    com_example_domain_b_g_y,
    com_example_domain_b_g_z,
    com_example_domain_b_h_x,
    com_example_domain_b_h_y,
    com_example_domain_b_h_z,
    com_example_domain_c_f_x,
    com_example_domain_c_f_y,
    com_example_domain_c_f_z,
    com_example_domain_c_g_x,
    com_example_domain_c_g_y,
    com_example_domain_c_g_z,
    com_example_domain_c_h_x,
    com_example_domain_c_h_y,
    com_example_domain_c_h_z
};

const char* to_cstring(Constants value);
std::string to_string(Constants value);
std::ostream& operator<<(std::ostream& out, Constants value);
std::istream& operator>>(std::istream& out, Constants& value);
std::array<Constants, 32> const& enum_list(Constants dummy);

} /* namespace domain */
} /* namespace example */
} /* namespace com */
