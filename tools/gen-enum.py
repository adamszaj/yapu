#!/usr/bin/env python3

import os
import sys
import CppGen
from optparse import OptionParser

class GenEnumConfig:
    def __init__(self):
        self.gen_class = True
        self.gen_flags = False
        self.gen_header_only = False

    pass

class GenEnum:
    def __init__(self, fullName, values, sourceName = None):
        self.fullName = fullName;
        names = fullName.split('.')
        self.namespaces = names[:-1]
        self.name = name[-1]
        self.values = values
        self.sourceName = sourceName
        pass

    def gen(self, dirName, baseName):
        pass

    pass

def gen_enum(source_name, lines, baseName, dirName, gen_class=True, gen_flags=False):
    if len(lines) > 1:
        fullName = lines[0]
        values = lines[1:]

        names = fullName.split('.')
        name  = names[-1]
        namespaces = names[:-1]
        rnamespaces = namespaces[:]
        rnamespaces.reverse()
        
        prefix=''
        if gen_class:
            prefix=name+'::'

        headerName  = dirName + '/' + baseName+'.h'
        cppName     = dirName + '/' + baseName+'.cpp'

        touscore = lambda v: '_'.join('_'.join(v.split('.')).split('-'))
        

        with open(headerName, 'w') as header:
            header.write('/**\n')
            header.write(' * GENERATED CODE FROM "'+source_name+'" FILE DO NOT MODIFY\n')
            header.write(' */\n')

            header.write('#pragma once\n')
            header.write('#include <string>\n')
            header.write('#include <iostream>\n')
            header.write('#include <array>\n')
            header.write('\n')

            if gen_flags:
                header.write('#include <ya/utils/flags.h>\n')
                header.write('\n')

            for ns in namespaces:
                header.write('namespace '+ns+' {\n')

            header.write('\n')
            header.write('enum ')
            if gen_class:
                header.write('class ')
            header.write(name + ' {\n')
            first = True
            for vn in values:
                if not first:
                    header.write(',\n')
                first = False
                header.write('    ' + touscore(vn))

            header.write('\n')
            header.write('};\n\n')

            header.write('const char* to_cstring('+name+' value);\n')
            header.write('std::string to_string('+name+' value);\n')

            header.write('std::ostream& operator<< (std::ostream& out, '+name+' value);\n')
            header.write('std::istream& operator>> (std::istream& out, '+name+'& value);\n')

            header.write('std::array<'+name+','+str(len(values))+'> const& enum_list('+name+' dummy);\n')
            header.write('\n')

            if gen_flags:
                header.write('using '+name+'Flags = ya::utils::flags<'+name+','+str(len(values))+'>;\n')

            header.write('\n')
            for ns in rnamespaces:
                header.write('} /* namespace '+ns+' */\n')
            header.write('\n')

        with open(cppName, 'w') as cpp:
            cpp.write('/**\n')
            cpp.write(' * GENERATED CODE FROM "'+source_name+'" FILE DO NOT MODIFY\n')
            cpp.write(' */\n')
            cpp.write('#include "'+baseName+'.h"\n')
            cpp.write('#include <algorithm>\n\n')

            if len(namespaces) > 0:
                for ns in namespaces:
                    cpp.write('namespace '+ns+' {\n')
                cpp.write('\n')


            cpp.write('namespace {\n')
            cpp.write('struct '+name+'_cmp {\n')
            cpp.write('    bool operator() ('+name+' lhs, std::string const& rhs) {\n')
            cpp.write('        return to_cstring(lhs) < rhs;\n')
            cpp.write('    }\n')

            cpp.write('    bool operator() (std::string const& lhs, '+name+' rhs) {\n')
            cpp.write('        return lhs < to_cstring(rhs);\n')
            cpp.write('    }\n')
            cpp.write('};\n')
            cpp.write('const char* const names[] = {\n')
            for vn in values:
                cpp.write('    "'+vn+'"')
                cpp.write(',\n')
            cpp.write('    nullptr')
            cpp.write('\n};\n')
            cpp.write('}\n\n')

            sortedValues = values[:]
            sortedValues.sort()

            cpp.write('std::array<'+name+','+str(len(values))+'> const& enum_list('+name+' dummy) {\n')
            cpp.write('    static const std::array<'+name+','+str(len(values))+'> __list {')

            coma=''
            for n in [prefix + touscore(v) for v in values]:
                cpp.write(coma+'\n        ' + n)
                coma=','
            cpp.write('\n    };\n')

            cpp.write('    return __list;\n')
            cpp.write('}\n\n')

            cpp.write('static std::array<'+name+','+str(len(values))+'> const& sorted_enum_list('+name+' dummy) {\n')
            cpp.write('    static const std::array<'+name+','+str(len(values))+'> __list {')

            coma=''
            for n in [prefix + touscore(v) for v in sortedValues]:
                cpp.write(coma+'\n        ' + n)
                coma=','
            cpp.write('\n    };\n')

            cpp.write('    return __list;\n')
            cpp.write('}\n\n')
            cpp.write('const char* to_cstring('+name+' value) {\n')
            cpp.write('    return names[static_cast<unsigned>(value)];\n')
            cpp.write('}\n')
            cpp.write('\n')
            cpp.write('std::string to_string('+name+' value) {\n')
            cpp.write('    return to_cstring(value);\n')
            cpp.write('}\n')
            cpp.write('\n')

            cpp.write('std::ostream& operator<< (std::ostream& out, '+name+' value) {\n')
            cpp.write('    return out << to_cstring(value);\n')
            cpp.write('}\n')
            cpp.write('\n')

    
            cpp.write('std::istream& operator>> (std::istream& in, '+name+'& value) {\n')
            cpp.write('    std::string s;\n')
            cpp.write('    if (in >> s) {\n')
            cpp.write('        '+name+'_cmp  cmp;\n')
            cpp.write('        auto const& sorted = sorted_enum_list('+name+'{});\n')
            cpp.write('        auto it = std::lower_bound(sorted.begin(), sorted.end(), s, cmp);\n')
            cpp.write('        if ((it != sorted.end()) && (to_cstring(*it) == s)) {\n')
            cpp.write('            value = *it;\n')
            cpp.write('            return in;\n')
            cpp.write('        }\n')
            cpp.write('    }\n')
            cpp.write('    in.clear(std::ios::failbit);\n')
            cpp.write('    return in;\n')
            cpp.write('}\n')

            if len(rnamespaces) > 0:
                cpp.write('\n')
                for ns in rnamespaces:
                    cpp.write('} /* namespace '+ns+' */\n')

        return True
    else:
        return False

def readFile(input):
    lines=[]
    for l in input.readlines():
        s = l.strip()
        if len(s) > 0 and s[0] != '#':
            lines.append(s)
    return lines



if __name__ == '__main__':

    parser = OptionParser()
    print('argv:', sys.argv)

    parser.add_option('-i', '--input', dest='enum_def', help='Input file for generator', metavar='ENUMDEF')
    parser.add_option('-o', '--output', dest='out_base', help='Output file base will produce OUTBASE.h and OUTBASE.cpp', metavar='OUTBASE')
    parser.add_option('-O', '--output-dir', dest='out_dir', help='Output directory for generated files', metavar='OUTDIR')
    parser.add_option('-n', '--no-class', dest='gen_class', action='store_false', default=True, help='Don\'t generate enum class')
    parser.add_option('-f', '--flags', dest='gen_flags', action='store_true', default=False, help='Generate typedef to ya::utils::flags<> based on generated Enum')

    (options, args) = parser.parse_args()

    if options.enum_def is None:
        print("no input file given") 
        inputFileName = 'stdin'
    else:
        inputFileName = options.enum_def

    if options.out_base is not None:
        baseName = options.out_base
    else:
        baseName = os.path.basename(inputFileName[:-5])

    if options.out_dir is not None:
        outputDirName = options.out_dir
    else:
        outputDirName = os.path.dirname(inputFileName)
        if len(outputDirName) is 0:
            outputDirName = '.'

    if inputFileName.endswith('.enum'):
        with open(inputFileName, 'r') as input:
            lines=readFile(input)
            gen_enum(inputFileName, lines, baseName, outputDirName, options.gen_class, options.gen_flags)
    else:
        print(inputFileName) 
        if inputFileName == 'stdin':
            lines=readFile(sys.stdin)
            gen_enum("stdin", lines, baseName, outputDirName, options.gen_class, options.gen_flags)
        else:
            print("bad file name format:",inputFileName) 
            
