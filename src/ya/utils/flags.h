#pragma once

#include <algorithm>
#include <bitset>

namespace ya {
namespace utils {
enum class flags_constants : std::size_t
{
    default_size = 64
};

template <class Enum, std::size_t N = static_cast<std::size_t>(flags_constants::default_size)>
class flags
{
  public:
    using flag_type = Enum;

    static constexpr std::size_t size = N;

    flags() = default;

    flags(unsigned long long val)
        : bset{val}
    {
    }

    flags(const std::string& sval, char zero = '0', char one = '1')
        : bset{sval, 0, std::string::npos, zero, one}
    {
    }

    flags(std::initializer_list<Enum> values)
        : bset{}
    {
        set(values);
    }

    void set()
    {
        bset.set();
    }

    void clear()
    {
        bset.reset();
    }

    void set(bool s)
    {
        if (s) {
            set();
        } else {
            clear();
        }
    }

    void set(Enum e)
    {
        bset.set(enumToIndex(e));
    }

    void clear(Enum e)
    {
        bset.reset(enumToIndex(e));
    }

    void set(Enum e, bool s)
    {
        if (s) {
            set(e);
        } else {
            clear(e);
        }
    }

    void flip(Enum e)
    {
        bset.flip(enumToIndex(e));
    }

    bool test(Enum e) const
    {
        return bset.test(enumToIndex(e));
    }

    bool all() const
    {
        return bset.all();
    }

    bool any() const
    {
        return bset.any();
    }

    bool none() const
    {
        return bset.none();
    }

    bool all_of(std::initializer_list<Enum> values) const
    {
        return std::all_of(values.begin(), values.end(), [this](Enum e) { return test(e); });
    }

    bool any_of(std::initializer_list<Enum> values) const
    {
        return std::any_of(values.begin(), values.end(), [this](Enum e) { return test(e); });
    }

    bool none_of(std::initializer_list<Enum> values) const
    {
        return std::none_of(values.begin(), values.end(), [this](Enum e) { return test(e); });
    }

    void set(std::initializer_list<Enum> values)
    {
        for (auto e : values)
            set(e);
    }

    void clear(std::initializer_list<Enum> values)
    {
        for (auto e : values)
            clear(e);
    }

    void flip(std::initializer_list<Enum> values)
    {
        for (auto e : values)
            flip(e);
    }

    std::string to_string() const
    {
        return bset.to_string();
    }

    friend std::ostream& operator<<(std::ostream& out, const flags<Enum, N>& fl)
    {
        return out << fl.bset;
    }

    friend std::istream& operator>>(std::istream& in, flags<Enum, N>& fl)
    {
        return in >> fl.bset;
    }

    unsigned long long to_ullong() const
    {
        return bset.to_ullong();
    }

    unsigned long to_ulong() const
    {
        return bset.to_ulong();
    }

  private:
    std::bitset<N> bset;
    std::size_t enumToIndex(Enum e) const
    {
        return static_cast<std::size_t>(e);
    }
};

template <class T, std::size_t N>
std::string to_string(const flags<T, N>& fl)
{
    return fl.to_string();
}

} /* namespace utils */
} /* namespace yapu */
