/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include <iostream>
#include <map>
#include <syslog.h>

namespace ya {
namespace utils {

std::ostream& log(int level, const char* file, const char* fun, unsigned int line);

void set_thread_name(const std::string& name);
std::string get_thread_name();

void set_logger(const std::string& progname, const std::string& logger);
int set_log_level(int level);
int get_log_level();

class log_output
{
  public:
    virtual ~log_output() = default;
    virtual void print(int level, const char* header, size_t hsize, const char* line, size_t lsize) noexcept = 0;
};

} /* namespace utils */
} /* namespace ya */

#define log_out(__level, msg)                                                                                          \
    do {                                                                                                               \
        if (ya::utils::get_log_level() >= __level) {                                                                   \
            ya::utils::log(__level, __FILE__, __FUNCTION__, __LINE__) << msg << std::flush;                            \
        }                                                                                                              \
    } while (0)

#define log_debug(msg) log_out(LOG_DEBUG, msg)
#define log_info(msg) log_out(LOG_INFO, msg)
#define log_notice(msg) log_out(LOG_NOTICE, msg)
#define log_warning(msg) log_out(LOG_WARNING, msg)
#define log_error(msg) log_out(LOG_ERR, msg)
#define log_crit(msg) log_out(LOG_CRIT, msg)
#define log_alert(msg) log_out(LOG_ALERT, msg)
#define log_emerg(msg) log_out(LOG_EMERG, msg)
#define log_call(call)                                                                                                 \
    do {                                                                                                               \
        log_debug("before: " #call);                                                                                   \
        call;                                                                                                          \
        log_debug("after: " #call);                                                                                    \
    } while (0)
