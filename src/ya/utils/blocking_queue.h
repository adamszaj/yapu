/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include "queue_code.h"
#include <atomic>
#include <condition_variable>
#include <deque>
#include <stdexcept>

namespace ya {
namespace utils {

/// exception
struct queue_error : std::exception
{
  private:
    queue_code code_;

  public:
    queue_error(queue_code code)
        : code_{code}
    {
    }

    queue_code code() const
    {
        return code_;
    }

    const char* what() const noexcept
    {
        return to_cstring(code_);
    }
};

struct queue_is_closed : queue_error
{
    queue_is_closed()
        : queue_error(queue_code::queue_is_closed)
    {
    }
};

struct queue_is_full : queue_error
{
    queue_is_full()
        : queue_error(queue_code::queue_is_full)
    {
    }
};

struct queue_is_empty : queue_error
{
    queue_is_empty()
        : queue_error(queue_code::queue_is_empty)
    {
    }
};

struct notify_one
{
    std::condition_variable& cv_;
    notify_one(std::condition_variable& cv)
        : cv_(cv)
    {
    }
    ~notify_one()
    {
        cv_.notify_one();
    }
};

struct notify_all
{
    std::condition_variable& cv_;
    notify_all(std::condition_variable& cv)
        : cv_(cv)
    {
    }
    ~notify_all()
    {
        cv_.notify_all();
    }
};

struct queue_block
{
    bool block;
    explicit queue_block(bool _block)
        : block(_block)
    {
    }
    operator bool() const
    {
        return block;
    }
};

template <class T>
class blocking_queue
{
  public:
    using value_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
    using unique_lock = std::unique_lock<std::mutex>;

    blocking_queue(std::size_t max_size = 0)
        : closed_{false}
        , max_size_{max_size}
    {
    }

    blocking_queue(blocking_queue&&) = delete;
    blocking_queue(const blocking_queue&) = delete;
    blocking_queue& operator=(blocking_queue&&) = delete;
    blocking_queue& operator=(const blocking_queue&) = delete;

    ~blocking_queue() = default;

    value_type pop(unique_lock& lock, queue_block block = queue_block{true})
    {

        _check_lock(lock);

        if (block) {
            while (!closed_ && q_.empty())
                empty_cv_.wait(lock);
        } else {
            if (!closed_ && q_.empty())
                throw queue_is_empty{};
        }

        if (q_.empty()) {
            throw queue_is_closed{};
        }

        notify_all noti{full_cv_};
        auto pop_front = make_pop_front(q_);
        return std::move(q_.front());
    }

    queue_code pop(unique_lock& lock, value_type& result, queue_block block = queue_block{true})
    {
        _check_lock(lock);
        try {
            result = pop(lock, block);
        } catch (const queue_error& e) {
            return e.code();
        }
        return queue_code::queue_ok;
    }

    /**
     * \brief pop
     */
    value_type pop(queue_block block = queue_block{true})
    {
        unique_lock lock{m_};
        return pop(lock, block);
    }

    /**
     * \brief pop
     */
    queue_code pop(value_type& result, queue_block block = queue_block{true})
    {
        unique_lock lock{m_};
        return pop(lock, result, block);
    }

    /**
     * \brief push of r-value
     */
    queue_code push(value_type&& v, queue_block block = queue_block{true})
    {
        unique_lock lock{m_};

        if (closed_)
            return queue_code::queue_is_closed;

        if (block) {
            while (max_size_ > 0 && q_.size() == max_size_)
                full_cv_.wait(lock);
        } else {
            if (max_size_ > 0 && q_.size() == max_size_)
                return queue_code::queue_is_full;
        }

        empty_cv_.notify_one();
        q_.emplace_back(std::move(v));
        return queue_code::queue_ok;
    }

    /**
     * \brief push of l-value
     */
    queue_code push(const value_type& v, queue_block block = queue_block{true})
    {
        return push(value_type{v}, block);
    }

    void close(unique_lock& lock, bool _clear = false)
    {
        _check_lock(lock);
        _close(_clear);
    }

    void close(bool _clear = false)
    {
        unique_lock lock{m_};
        _close(_clear);
    }

    void wait(unique_lock& lock)
    {
        _check_lock(lock);
        _wait(lock);
    }

    void wait()
    {
        unique_lock lock{m_};
        _wait(lock);
    }

    template <class Clock, class Duration>
    bool wait_until(unique_lock& lock, const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        _check_lock(lock);
        return _wait_until(lock, timeout_time);
    }

    template <class Rep, class Period>
    bool wait_for(unique_lock& lock, const std::chrono::duration<Rep, Period>& rel_time)
    {
        return wait_until(lock, std::chrono::steady_clock::now() + rel_time);
    }

    template <class Clock, class Duration>
    bool wait_until(const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        unique_lock lock{m_};
        return _wait_until(lock, timeout_time);
    }

    template <class Rep, class Period>
    bool wait_for(const std::chrono::duration<Rep, Period>& rel_time)
    {
        return wait_until(std::chrono::steady_clock::now() + rel_time);
    }

    bool closed(unique_lock& lock) const
    {
        _check_lock(lock);
        return closed_;
    }

    bool closed() const
    {
        unique_lock lock{m_};
        return closed_;
    }

    bool empty(unique_lock& lock) const
    {
        _check_lock(lock);
        return q_.empty();
    }

    bool empty() const
    {
        unique_lock lock{m_};
        return q_.empty();
    }

    void clear(unique_lock& lock)
    {
        _check_lock(lock);
        _clear();
    }

    void clear()
    {
        unique_lock lock{m_};
        clear(lock);
    }

    unique_lock make_lock()
    {
        return unique_lock{m_};
    }

  private:
    bool closed_;
    std::size_t max_size_;
    std::deque<value_type> q_;
    mutable std::mutex m_;
    std::condition_variable empty_cv_;
    std::condition_variable full_cv_;

    void _close(bool _clear)
    {
        notify_all notie{empty_cv_};
        notify_all notif{full_cv_};

        closed_ = true;
        if (_clear) {
            q_.clear();
        }
    }

    void _clear()
    {
        notify_all noti{full_cv_};
        q_.clear();
    }

    void _wait(unique_lock& lock)
    {
        full_cv_.wait(lock, [this] { return q_.empty(); });
    }

    template <class Clock, class Duration>
    bool _wait_until(unique_lock& lock, const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        return full_cv_.wait_until(lock, timeout_time, [this] { return q_.empty(); });
    }

    void _check_lock(unique_lock& lock) const
    {
        if (lock.mutex() != &m_)
            throw std::logic_error{"Bad lock object"};
        if (!lock.owns_lock())
            throw std::logic_error{"Given lock object does not owns lock"};
    }

    /**
     * this object call pop_front in destructor which may be after return
     * statement
     */
    template <class C>
    struct _PopFront
    {
        C& c;
        _PopFront(C& _c)
            : c(_c)
        {
        }
        ~_PopFront()
        {
            c.pop_front();
        }
    };

    template <class C>
    _PopFront<C> make_pop_front(C& c)
    {
        return {c};
    }
};
}
}
