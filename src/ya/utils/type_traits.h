#pragma once
namespace ya {
namespace utils {

template <class T>
struct member_function_owner_class;
template <class R, class C, class... Args>
struct member_function_owner_class<R (C::*)(Args...)>
{
    using type = C;
};
} /* namespace utils */
} /* namespace ya */
