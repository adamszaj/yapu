/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include <sstream>
#include <string>
#include <typeinfo>

namespace ya {
namespace utils {

/**
 *  convert every kind of object to its string representation using operator <<
 * (ostream, T).
 *  @param v object to be converted
 */
template <class T>
std::string to_string(const T& v)
{
    std::ostringstream out;
    out << v;
    return out.str();
}

/**
 * convert string to given type using operator >> (istream, T)
 * @param s string representation of object of type T
 * @return value of type T
 * @throw std::invalid_argument if string can't be converted to T
 */
template <class T>
T from_string(const std::string& s)
{
    T v{};
    std::istringstream is{s};
    if (is >> v) {
        return v;
    }
    throw std::invalid_argument{"can not convert '" + s + "' to type: " + typeid(T).name()};
}
}
}
