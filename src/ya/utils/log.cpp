/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "log.h"
#include "lock.h"
#include "string.h"

#include <cstring>
#include <memory>
#include <mutex>
#include <sstream>
#include <strings.h>
#include <sys/uio.h>
#include <thread>
#include <utility>
#include <vector>

#ifdef WITH_CXXUX
#include "cxxux/sys/file.h"
#endif

namespace ya {
namespace utils {

namespace /* annonumous */ {

const std::string& __get_thread_name();

#ifdef WITH_CXXUX
using namespace cxxux::sys;
class file_output : public log_output
{
  public:
    file_output(cxxux::sys::unique_file f)
        : output(std::move(f))
    {
    }

    void print(int level, const char* header, size_t hsize, const char* line, size_t lsize) noexcept
    {
        char lf[] = "\n";
        const struct iovec iov[3] = {{const_cast<void*>(static_cast<const void*>(header)), hsize},
                                     {const_cast<void*>(static_cast<const void*>(line)), lsize},
                                     {lf, 1}};
        try {
            output.writev(iov, 3);
        } catch (...) {
        } //< catch all exceptions... don't log the logger errors
    }

  private:
    cxxux::sys::unique_file output;
};
#endif

class stderr_output : public log_output
{
  public:
    void print(int level, const char* header, size_t hsize, const char* line, size_t lsize) noexcept
    {
        char lf[] = "\n";
        const struct iovec iov[3] = {{const_cast<void*>(static_cast<const void*>(header)), hsize},
                                     {const_cast<void*>(static_cast<const void*>(line)), lsize},
                                     {lf, 1}};
        if (::writev(2, iov, 3) == -1) { /* quiet */
        }
    }
};

class syslog_output : public log_output
{
  private:
    std::string progname;

  public:
    syslog_output(const std::string& ident,
                  int option = LOG_NDELAY | LOG_PERROR | LOG_PID | LOG_CONS,
                  int facility = LOG_USER)
        : progname(ident)
    {

        openlog(progname.c_str(), option, facility);
        char line[] = "connecting to the syslog";
        print(LOG_NOTICE, nullptr, 0, line, std::strlen(line));
    }

    ~syslog_output()
    {
        char line[] = "disconnecting from the syslog";
        print(LOG_NOTICE, nullptr, 0, line, std::strlen(line));
        closelog();
    }

    void print(int level, const char* header, size_t hsize, const char* line, size_t lsize) noexcept
    {
        if (header && (hsize > 0) && line && (lsize > 0)) {
            char format[12] = "%s%s";
            syslog(level, format, header, line);
        } else if (line && (lsize > 0)) {
            char format[4] = "%s";
            syslog(level, format, line);
        }
    }
};

bool starts_with(const std::string& s, const std::string& search)
{
    return (s.size() >= search.size()) && s.substr(0, search.size()) == search;
}

class global_logger : public log_output
{
  public:
    global_logger()
    {
        set_output("", "stderr:");
    }
    ~global_logger()
    {
    }

    void set_output(const std::string& progname, const std::string& logger_uri)
    {
        std::unique_ptr<log_output> out;
        if (starts_with(logger_uri, "syslog:")) {
            out = std::make_unique<syslog_output>(progname);
        } else if (starts_with(logger_uri, "stderr:")) {
            out = std::make_unique<stderr_output>();
        }

        std::lock_guard<ya::utils::spin_lock> lock{slock};
        output = std::move(out);
    }

    void print(int level, const char* header, size_t hsize, const char* line, size_t lsize) noexcept
    {
        std::lock_guard<ya::utils::spin_lock> lock{slock};
        output->print(level, header, hsize, line, lsize);
    }

  private:
    std::unique_ptr<log_output> output;
    ya::utils::spin_lock slock;
};

int log_level = LOG_ERR;

global_logger glogger;

const char* level_names[]{"emerg", "alert", "crit", "error", "warning", "notice", "info", "debug", nullptr};

template <class CharT>
class log_streambuf : public std::basic_streambuf<CharT>
{
  public:
    using super = std::basic_streambuf<CharT>;
    using char_type = typename super::char_type;
    using int_type = typename super::int_type;

    log_streambuf(std::streamsize size = 1024)
        : level{LOG_INFO}
        , header(120)
        , outbuf()
    {
        outbuf.reserve(size);
    }

    ~log_streambuf()
    {
    }

    void set_header(int __level, const char* file, const char* fun, unsigned int line)
    {
        const char* base_file = std::strrchr(file, '/');
        level = __level;
        if (!base_file) {
            base_file = file;
        } else {
            ++base_file;
        }
        snprintf(header.data(),
                 header.size(),
                 "[%s.%s] %s:%u:%s() *** ",
                 __get_thread_name().c_str(),
                 level_names[__level],
                 base_file,
                 line,
                 fun);
    }

  protected:
    bool is_flush_char(int_type ch) const
    {
        return (ch == super::traits_type::eof() || ch == '\n' || ch == '\r');
    }

    int_type overflow(int_type ch)
    {
        if (is_flush_char(ch)) {
            if (outbuf.size() > 0) {
                outbuf.push_back(0);
                glogger.print(level, header.data(), std::strlen(header.data()), outbuf.data(), outbuf.size() - 1);
                outbuf.resize(0);
            }
        } else {
            outbuf.push_back(ch);
        }
        return ch;
    }

    int_type sync()
    {
        overflow('\n');
        return 0;
    }

    int_type underflow()
    {
        return super::traits_type::eof();
    }
    std::streamsize showmanyc()
    {
        return 0;
    }

  private:
    int level;
    std::vector<CharT> header;
    std::vector<CharT> outbuf;
};

struct thread_data
{
    std::string name;
    log_streambuf<char> log_sb;
    std::ostream log;

    thread_data()
        : name{}
        , log_sb{}
        , log(&log_sb)
    {
    }
};

static thread_local std::unique_ptr<thread_data> thread_data_ptr;

static thread_data* get_thread_data()
{
    if (!thread_data_ptr)
        thread_data_ptr = std::make_unique<thread_data>();
    return thread_data_ptr.get();
}

const std::string& __get_thread_name()
{

    if (get_thread_data()->name.empty())
        get_thread_data()->name = ya::utils::to_string(std::this_thread::get_id());
    return get_thread_data()->name;
}

} /* namespace annonumous */

int set_log_level(int level)
{
    int old = log_level;
    log_level = level;
    return old;
}

int get_log_level()
{
    return log_level;
}

void set_logger(const std::string& progname, const std::string& logger)
{
    glogger.set_output(progname, logger);
}

void set_thread_name(const std::string& name)
{
    get_thread_data()->name = name;
}

std::string get_thread_name()
{
    return __get_thread_name();
}

std::ostream& log(int level, const char* file, const char* fun, unsigned int line)
{
    auto data = get_thread_data();
    data->log_sb.set_header(level, file, fun, line);
    return data->log;
}

} /* namespace utils */
} /* namespace ya */
