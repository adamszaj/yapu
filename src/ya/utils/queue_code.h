/**
 * GENERATED CODE FROM "queue_code.enum" FILE DO NOT MODIFY
 */
#pragma once
#include <array>
#include <iostream>
#include <string>

#include <ya/utils/flags.h>

namespace ya {
namespace utils {

enum class queue_code
{
    queue_ok,
    queue_is_empty,
    queue_is_full,
    queue_is_closed
};

const char* to_cstring(queue_code value);
std::string to_string(queue_code value);
std::ostream& operator<<(std::ostream& out, queue_code value);
std::istream& operator>>(std::istream& out, queue_code& value);
std::array<queue_code, 4> const& enum_list(queue_code dummy);

using queue_codeFlags = ya::utils::flags<queue_code, 4>;

} /* namespace utils */
} /* namespace ya */
