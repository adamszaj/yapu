/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include <limits>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <ya/utils/string.h>
#include <ya/utils/flags.h>

#include "OccurrenceRange.h"

namespace ya {
namespace app {
namespace options {

enum class OptionParameter
{
    none,
    optional,
    required
};

class Description
{
  public:
    Description() = default;
    Description(Description const&) = default;
    Description(Description&&) = default;

    Description& operator=(Description const&) = default;
    Description& operator=(Description&&) = default;

    Description(std::string const& _short_desc, std::string const& _long_desc = {})
        : short_desc_{_short_desc}
        , long_desc_{_long_desc}
    {
    }
    std::string const& getShortDescription() const noexcept
    {
        return short_desc_;
    }

    std::string const& getLongDescription() const noexcept
    {
        return long_desc_;
    }

  private:
    std::string short_desc_;
    std::string long_desc_;
};

class Option
{
  public:
    Option(const OccurrenceRange& o = ZeroOrOne{})
        : occur_range_{o}
    {
    }

    virtual ~Option() = default;
    /**
     * @return number of occurencies
     */
    unsigned count() const
    {
        return count_;
    }

    int position() const
    {
        return position_;
    }

    void position(int _pos)
    {
        position_ = _pos;
    }

    OptionParameter parameter() const
    {
        return parameter_;
    }

    OptionParameter set(OptionParameter p)
    {
        std::swap(p, parameter_);
        return p;
    }

    void set(Description const& _description)
    {
        description_ = _description;
    }

    void set(OccurrenceRange const& _occur_range)
    {
        occur_range_ = _occur_range;
    }

    virtual void operator()(const std::string& arg) = 0;

    void setShortName(char o)
    {
        short_opt_ = o;
    }

    void setLongName(std::string const& s)
    {
        long_opt_ = s;
    }

    char getShortName() const
    {
        return short_opt_;
    }

    std::string const& getLongName() const
    {
        return long_opt_;
    }

    Description const& getDescription() const
    {
        return description_;
    }

  private:
    std::string long_opt_;
    char short_opt_;
    size_t count_{};
    int position_{-1};
    OccurrenceRange occur_range_;
    OptionParameter parameter_{OptionParameter::none};
    Description description_;
};

template <class T>
class StoreValueOption : public Option
{
  public:
    StoreValueOption(T& v, const OccurrenceRange& o = ZeroOrOne{})
        : Option(o)
        , value(v)
    {
    }

    void operator()(const std::string& arg) override
    {
        using ya::utils::from_string;
        value = from_string<T>(arg);
    }

  private:
    T& value;
};

template <class T>
class StoreValueOption<std::vector<T>> : public Option
{
  public:
    StoreValueOption(std::vector<T>& v, const OccurrenceRange& o = ZeroOrMore{})
        : Option(o)
        , values(v)
    {
    }

    void operator()(const std::string& arg) override
    {
        using ya::utils::from_string;
        values.push_back(from_string<T>(arg));
    }

  private:
    std::vector<T>& values;
};

template <>
class StoreValueOption<std::string> : public Option
{
  public:
    StoreValueOption(std::string& s, const OccurrenceRange& o = ZeroOrOne{})
        : Option(o)
        , str(s)
    {
    }
    void operator()(const std::string& arg) override
    {
        str = arg;
    }

  private:
    std::string& str;
};

template <>
class StoreValueOption<bool> : public Option
{
  public:
    StoreValueOption(bool& _opt, bool _store_value, const OccurrenceRange& o = ZeroOrOne{})
        : Option(o)
        , opt_(_opt)
        , store_value_{_store_value}
    {
    }

    void operator()(const std::string& arg) override
    {
        opt_ = store_value_;
    }

  private:
    bool& opt_;
    bool const store_value_;
};

template <class T, std::size_t N>
class SetFlagOption : public Option
{
  public:
    SetFlagOption(ya::utils::flags<T, N>& _flags, T _flag, bool _store_value)
        : flags_(_flags)
        , flag_{_flag}
        , store_value_{_store_value}
    {
    }

    void operator()(const std::string& arg) override
    {
        flags_.set(flag_, store_value_);
    }

  private:
    ya::utils::flags<T, N>& flags_;
    T const flag_;
    bool const store_value_;
};

template <>
class StoreValueOption<std::vector<std::string>> : public Option
{
  public:
    StoreValueOption(std::vector<std::string>& sv, const OccurrenceRange& o = ZeroOrMore{})
        : Option(o)
        , strv(sv)
    {
    }
    void operator()(const std::string& arg) override
    {
        strv.push_back(arg);
    }

  private:
    std::vector<std::string>& strv;
};

template <class T>
std::unique_ptr<StoreValueOption<T>> make_param_option(T& v)
{
    auto p = std::make_unique<StoreValueOption<typename std::decay<T>::type>>(v);
    p->set(OptionParameter::required);
    return p;
}

template <class T>
std::unique_ptr<StoreValueOption<T>> make_param_option(T& v, const T& default_value)
{
    auto p = std::make_unique<StoreValueOption<typename std::decay<T>::type>>(v);
    p->set(OptionParameter::optional);
    return p;
}

std::unique_ptr<StoreValueOption<bool>> make_option(bool& v, bool _store_value = true)
{
    auto p = std::make_unique<StoreValueOption<bool>>(v, _store_value);
    p->set(OptionParameter::none);
    return p;
}

template <class T, std::size_t N>
std::unique_ptr<SetFlagOption<T, N>> make_option(ya::utils::flags<T, N>& _flags, T _flag, bool _store_value = true)
{
    auto p = std::make_unique<SetFlagOption<T, N>>(_flags, _flag, _store_value);
    p->set(OptionParameter::none);
    return p;
}

class ProgramOptions
{
  public:
    using OptionPtr = std::unique_ptr<Option>;
    using Options = std::vector<OptionPtr>;
    using Iterator = Options::iterator;
    using ConstIterator = Options::const_iterator;

    ProgramOptions()
        : positional_(1)
    {
    }

    struct position_any
    {
    };

    ProgramOptions& addPositional(std::unique_ptr<Option>&& option)
    {
        positional_.emplace_back(std::move(option));
        return *this;
    }

    ProgramOptions& addPositional(std::unique_ptr<Option>&& option, position_any any)
    {
        positional_[0] = std::move(option);
        return *this;
    }

    template <class Arg, class... Args>
    ProgramOptions& addOption(std::string const& opt_name, std::unique_ptr<Option>&& option, Arg&& arg, Args&&... args)
    {
        option->set(std::forward<Arg>(arg));
        return addOption(opt_name, std::move(option), std::forward<Args>(args)...);
    }

    ProgramOptions& addOption(std::string const& opt_name, std::unique_ptr<Option>&& option)
    {
        auto names = parseName(opt_name);
        auto index = options_.size();
        if (names.second) {
            short_map_.insert(std::make_pair(names.second, index));
            option->setShortName(names.second);
        }
        if (!names.first.empty()) {
            long_map_.insert(std::make_pair(names.first, index));
            option->setLongName(names.first);
        }
        options_.emplace_back(std::move(option));
        return *this;
    }

    void parse(const std::vector<std::string>& args)
    {
        unsigned position{1};

        for (unsigned i = 1; i < args.size(); ++i) {
            auto& a = args[i];
            if (a.size() > 2 && a[0] == '-' && a[1] == '-') {
                auto eq_pos = a.find('=');
                size_t n = a.size() - 2;
                if (eq_pos != std::string::npos) {
                    n = eq_pos - 2;
                }
                auto opt = a.substr(2, n);
                auto option = find(opt);
                if (option) {
                    i = runOption(args, i, option, eq_pos);
                } else {
                    throw std::logic_error("Bad option: '" + opt + "'");
                }
            } else if (a.size() > 1 && a[0] == '-') {
                for (std::string::size_type si = 1; si < a.size(); ++si) {
                    auto option = find(a[si]);
                    if (option) {
                        i = runOption(args, i, option, si);
                    } else {
                        std::stringstream os;
                        os << "Bad option: '" << a[si] << "'";
                        throw std::logic_error(os.str());
                    }
                }
            } else {
                bool consumed{false};
                if (position < positional_.size()) {
                    if (positional_[position]) {
                        consumed = true;
                        (*positional_[position])(a);
                    }
                }
                if (!consumed && positional_[0]) {
                    (*positional_[0])(a);
                }
                ++position;
            }
        }
    }

    template <class Enum, std::size_t N>
    ProgramOptions& addOptions(ya::utils::flags<Enum, N>& _flags,
                               std::string const& _negativePrefix = {},
                               std::string const& _positivePrefix = {})
    {
        using ya::app::options::Description;
        for (auto option : enum_list(Enum{})) {
            addOption(_positivePrefix + to_string(option), make_option(_flags, option), Description{to_string(option)});
            if (!_negativePrefix.empty()) {
                addOption(_negativePrefix + to_string(option),
                          make_option(_flags, option, false),
                          Description{to_string(option)});
            }
        }
        return *this;
    }

    ConstIterator cbegin() const
    {
        return begin();
    }

    ConstIterator cend() const
    {
        return end();
    }

    ConstIterator begin() const
    {
        return options_.begin();
    }

    ConstIterator end() const
    {
        return options_.end();
    }

    ConstIterator begin()
    {
        return options_.begin();
    }

    ConstIterator end()
    {
        return options_.end();
    }

  private:
    unsigned runOption(const std::vector<std::string>& args, unsigned i, Option* option, std::string::size_type& eq_pos)
    {
        std::string param{};
        bool has_param{false};

        switch (option->parameter()) {
            case OptionParameter::required:
                if (eq_pos == std::string::npos || ((eq_pos + 1) == args[i].size())) {
                    if (i + 1 < args.size()) {
                        ++i;
                        param = args[i];
                        has_param = true;
                    } else {
                        throw std::logic_error("no arg for option");
                    }
                }
            case OptionParameter::optional:
                if (!has_param) {
                    if (eq_pos != std::string::npos) {
                        if ((eq_pos + 1) < args[i].size()) {
                            param = args[i].substr(eq_pos + 1);
                            eq_pos = args[i].size();
                            has_param = true;
                        }
                    }
                }
                (*option)(param);
                break;
            case OptionParameter::none:
                if ((eq_pos != std::string::npos) && (args[i][eq_pos] == '=')) {
                    throw std::logic_error("option requires no argument");
                }
                (*option)(param);
                break;
        }
        return i;
    }

    std::pair<std::string, char> parseName(std::string const& opt_name)
    {
        auto pos = opt_name.find(',');
        if (pos != std::string::npos) {
            if ((pos + 2) == opt_name.size()) {
                return {opt_name.substr(0, pos), opt_name[pos + 1]};
            }
            throw std::invalid_argument("Bad option name: " + opt_name);
        }
        return {opt_name, '\0'};
    }

    Option* find(char c)
    {
        auto it = short_map_.find(c);
        if (it != short_map_.end()) {
            return options_[it->second].get();
        }
        return nullptr;
    }

    Option* find(std::string const& s)
    {
        auto it = long_map_.find(s);
        if (it != long_map_.end()) {
            return options_[it->second].get();
        }
        throw std::logic_error("Bad option: " + s);
    }

    std::map<char, std::size_t> short_map_;
    std::map<std::string, std::size_t> long_map_;
    std::vector<OptionPtr> options_;
    std::vector<OptionPtr> positional_;
};

std::ostream& operator<<(std::ostream& out, ProgramOptions const& po)
{
    std::size_t max_len{};
    for (auto const& option : po) {
        std::ostringstream os;
        auto short_opt = option->getShortName();
        os << "    --" << option->getLongName();
        if (short_opt) {
            os << ", -" << short_opt;
        }
        auto len = os.str().size();
        if (len > max_len)
            max_len = len;
    }

    out << "Program Options\n";
    for (auto const& option : po) {
        std::ostringstream os;
        auto short_opt = option->getShortName();
        os << "    --" << option->getLongName();
        if (short_opt) {
            os << ", -" << short_opt;
        }
        out << std::left << std::setw(max_len) << os.str() << "    " << option->getDescription().getShortDescription();
        out << "\n";
    }
    return out;
}

} /* namespace options */
} /* namespace app */
} /* namespace ya */
