#pragma once

namespace ya {
namespace app {
namespace options {

class OccurrenceRange
{
  public:
    OccurrenceRange(size_t _min, size_t _max)
        : min(_min)
        , max(_max)
    {
    }
    OccurrenceRange(const OccurrenceRange& oc) = default;
    OccurrenceRange& operator=(const OccurrenceRange& oc) = default;

  private:
    size_t min;
    size_t max;
};

/// ?
struct ZeroOrOne : OccurrenceRange
{
    ZeroOrOne()
        : OccurrenceRange{0, 1}
    {
    }
};

/// {1}
struct One : OccurrenceRange
{
    One()
        : OccurrenceRange{1, 1}
    {
    }
};

/// *
struct ZeroOrMore : OccurrenceRange
{
    ZeroOrMore()
        : OccurrenceRange{0, std::numeric_limits<size_t>::max()}
    {
    }
};

/// +
struct OneOrMore : OccurrenceRange
{
    OneOrMore()
        : OccurrenceRange{0, std::numeric_limits<size_t>::max()}
    {
    }
};

} /* namespace options */
} /* namespace app */
} /* namespace ya */
