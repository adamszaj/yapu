/**
 * GENERATED CODE FROM "ThreadPoolCode.enum" FILE DO NOT MODIFY
 */
#pragma once
#include <array>
#include <iostream>
#include <string>

namespace ya {
namespace app {

enum class ThreadPoolCode
{
    queue_ok,
    queue_is_empty,
    queue_is_full,
    queue_is_closed,
    queue_not_found
};

const char* to_cstring(ThreadPoolCode value);
std::string to_string(ThreadPoolCode value);
std::ostream& operator<<(std::ostream& out, ThreadPoolCode value);
std::istream& operator>>(std::istream& out, ThreadPoolCode& value);
std::array<ThreadPoolCode, 5> const& enum_list(ThreadPoolCode dummy);

} /* namespace app */
} /* namespace ya */
