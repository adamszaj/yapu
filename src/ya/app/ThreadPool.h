/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include "ThreadPoolCode.h"

namespace ya {
namespace app {

template <class AppExec>
class ThreadPool
{
  public:
    using AppExecType = AppExec;
    using AppExecTrait = typename AppExecType::AppExecTrait;
    using TaskPtr = typename AppExecType::TaskPtr;

    class TaskCanceller
    {
      public:
        TaskCanceller() = default;

        /// enable move ctor/assign
        TaskCanceller(TaskCanceller&&) = default;
        TaskCanceller& operator=(TaskCanceller&&) = default;

        /// disable move ctor/assign
        TaskCanceller(const TaskCanceller&) = delete;
        TaskCanceller& operator=(const TaskCanceller&) = delete;

        TaskCanceller(const TaskPtr& _task)
            : task{_task}
        {
        }

        TaskCanceller(TaskPtr&& _task)
            : task{std::move(_task)}
        {
        }

        ~TaskCanceller()
        {
            if (task)
                task->cancel();
        }

        TaskPtr release()
        {
            TaskPtr res;
            res.swap(task);
            return res;
        }

      private:
        TaskPtr task;
    };

    using QueueType = ya::utils::blocking_queue<TaskCanceller>;
    using PoolIdType = typename AppExecType::PoolIdType;

    ThreadPool(PoolIdType _id, AppExecType& appe)
        : pool_id_(_id)
        , app_exec_(appe)
        , queue_{appe.getTraits().maxQueueSize(pool_id_)}
        , workers_{appe.getTraits().number(pool_id_)}
    {
        try {
            unsigned id{};

            for (auto& w : workers_)
                w = std::make_unique<Worker>(*this, id++);

        } catch (...) {
            // avoid blocking in worker dtor at join()
            queue_.close();
            throw;
        }
    }

    ~ThreadPool()
    {
        ThreadPool::close();
        ThreadPool::cancelAll();
        ThreadPool::wait();
    }

    PoolIdType id() const
    {
        return pool_id_;
    };

    QueueType& getQueue()
    {
        return queue_;
    }

    ThreadPoolCode push(const TaskPtr& task)
    {
        return toThreadPoolCode(getQueue().push(task));
    }

    AppExecType& getAppExec() const
    {
        return app_exec_;
    }

    void close()
    {
        queue_.close();
    }

    void cancelPending()
    {
        queue_.clear();
    }

    void cancelAll()
    {
        cancelPending();
        for (auto& w : workers_)
            w->cancel();
    }

    void wait()
    {
        queue_.wait();
        for (auto& w : workers_)
            w->wait();
    }

    template <class Clock, class Duration>
    void waitUntil(const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        if (queue_.wait_until(timeout_time)) {
            for (auto& w : workers_)
                w->waitUntil(timeout_time);
        }
    }

    template <class Rep, class Period>
    void waitFor(const std::chrono::duration<Rep, Period>& rel_time)
    {
        auto timeout_time = std::chrono::steady_clock::now() + rel_time;
        closeAndWaitUntil(timeout_time);
    }

  private:
    class Worker
    {
      public:
        Worker(ThreadPool& pool, unsigned id)
            : pool_(pool)
            , id_{id}
            , th_(&Worker::loop, this)
        {
        }

        Worker(const Worker&) = delete;
        Worker& operator=(const Worker&) = delete;

        Worker(Worker&&) = delete;
        Worker& operator=(Worker&&) = delete;

        using UniqueLock = std::unique_lock<std::mutex>;

        template <class Clock, class Duration>
        void waitUntil(const std::chrono::time_point<Clock, Duration>& timeout_time)
        {
            auto lock = getQueue().make_lock();
            while (!getQueue().empty(lock) || current_task_) {
                if (cond_.wait_until(lock, timeout_time) == std::cv_status::timeout)
                    break;
            }
        }

        void wait()
        {
            auto lock = getQueue().make_lock();
            while (!getQueue().empty(lock) || current_task_) {
                cond_.wait(lock);
            }
        }

        void cancel() noexcept
        {
            auto lock = getQueue().make_lock();
            if (current_task_)
                current_task_->cancel();
        }

        ~Worker()
        {
            if (th_.joinable()) {
                th_.join();
            }
        }

      private:
        ThreadPool& pool_;
        unsigned id_;
        TaskPtr current_task_;
        std::condition_variable cond_;
        std::thread th_;

        QueueType& getQueue()
        {
            return pool_.getQueue();
        }

        void loop()
        {
            auto& appExec = pool_.getAppExec();
            auto& traits = appExec.getTraits();
            traits.onThreadInit(pool_.id(), id_);

            auto& queue = pool_.getQueue();
            TaskCanceller taskCanceller;

            try {
                auto lock = queue.make_lock();

                while (queue.pop(lock, taskCanceller) == ya::utils::queue_code::queue_ok) {
                    current_task_ = taskCanceller.release();
                    lock.unlock();
                    current_task_->exec(appExec, current_task_);
                    lock.lock();
                    current_task_.reset();
                }
                cond_.notify_all();
            } catch (...) {
                /// TODO traits.onThreadLoopException()
            }

            traits.onThreadDestroy(pool_.id(), id_);
        }
    };

    using WorkerPtr = std::unique_ptr<Worker>;

    const PoolIdType pool_id_;
    AppExecType& app_exec_;
    QueueType queue_;
    std::vector<WorkerPtr> workers_;

    ThreadPoolCode toThreadPoolCode(ya::utils::queue_code code)
    {
        return static_cast<ThreadPoolCode>(code);
    }
};

} /* namespace app */
} /* namespace ya */
