/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once
#include <algorithm>
#include <memory>
#include <mutex>
#include <tuple>
#include <utility>
#include <vector>

#include <ya/utils/lock.h>
#include <ya/utils/type_traits.h>

namespace ya {
namespace app {

template <class InputIt, class UnaryPredicate, class Callable>
std::size_t call_if(InputIt first, InputIt last, UnaryPredicate p, Callable&& call)
{
    std::size_t count{};
    for (; first != last; ++first) {
        if (p(*first)) {
            call(*first);
            ++count;
        }
    }
    return count;
}

template <class L, class M = ya::utils::dummy_lock>
class EventDispatcher
{
  public:
    using MutexType = M;
    using LockGuard = std::lock_guard<MutexType>;
    using ListenerType = L;
    using ListenerPointer = typename std::add_pointer<ListenerType>::type;

    virtual ~EventDispatcher() = default;

    void addListener(ListenerPointer l)
    {
        LockGuard lock{m_};
        addListenerNoLock(l);
    }

    void removeListener(ListenerPointer l)
    {
        LockGuard lock{m_};
        remListenerNoLock(l);
    }

    // TODO dispatchAsync(AppExec&, PoolId, MF, Args)
    //

    template <class MF, class... Args>
    void dispatchSync(MF mf, Args&&... args)
    {
        static_assert(std::is_member_function_pointer<MF>::value, "Given argument is not a member function pointer");

        static_assert(std::is_base_of<ListenerType, typename ya::utils::member_function_owner_class<MF>::type>::value,
                      "Bad function pointer class");

        LockGuard lock{m_};
        using namespace std::placeholders;
        auto cnt = callForNotNulls(std::bind(mf, _1, std::forward<Args>(args)...));

        if (cnt < listeners_.size())
            cleanup();
    }

  protected:
    virtual void onListenerAdded(ListenerPointer lst)
    {
    }
    virtual void onListenerRemoved(ListenerPointer lst)
    {
    }

    void addListenerNoLock(ListenerPointer l)
    {
        listeners_.push_back(l);
        onListenerAdded(l);
    }

    void removeListenerNoLock(ListenerPointer l)
    {
        for (std::size_t i = 0; i < listeners_.size(); ++i) {
            if (l == listeners_[i]) {
                listeners_[i] = nullptr;
                onListenerRemoved(l);
            }
        }
    }

  private:
    MutexType m_;
    std::vector<ListenerPointer> listeners_;

    template <class Callable>
    std::size_t callForNotNulls(Callable&& c)
    {
        using namespace std::placeholders;

        static const auto if_not_null = std::bind(std::not_equal_to<ListenerPointer>{}, nullptr, _1);

        return call_if(listeners_.begin(), listeners_.end(), if_not_null, std::forward<Callable>(c));
    }

    void cleanup()
    {
        auto new_end = std::remove(listeners_.begin(), listeners_.end(), nullptr);
        auto old_end = listeners_.end();
        if (new_end != old_end) {
            listeners_.erase(new_end, old_end);
        }
    }
};
}
}
