/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include "ThreadPoolCode.h"
#include <unordered_map>

namespace ya {
namespace app {

enum class TimerType
{
    unknown,
    system,
    steady
};

static inline std::ostream& operator<<(std::ostream& out, TimerType t)
{
    switch (t) {
        case TimerType::unknown:
            return out << "unknown";
        case TimerType::system:
            return out << "system";
        case TimerType::steady:
            return out << "steady";
    }
    return out;
}
template <TimerType type>
struct TimerTraits
{
};

template <>
struct TimerTraits<TimerType::system>
{
    static constexpr TimerType timer_type{TimerType::system};
    using clock_type = std::chrono::system_clock;
    using time_point = clock_type::time_point;
};

template <>
struct TimerTraits<TimerType::steady>
{
    static constexpr TimerType timer_type{TimerType::steady};
    using clock_type = std::chrono::steady_clock;
    using time_point = clock_type::time_point;
};

class TimerId
{
  public:
    using IdType = uint64_t;
    using Type = TimerType;

    TimerId()
        : type{TimerType::unknown}
        , id{0}
    {
    }

    TimerId(Type _type, IdType _id)
        : type{_type}
        , id{_id}
    {
    }

    void reset()
    {
        type = TimerType::unknown;
        id = 0;
    }

    Type getType() const noexcept
    {
        return type;
    }

    IdType getId() const noexcept
    {
        return id;
    }

  private:
    Type type;
    IdType id;
};

template <class AppExec>
class Timer
{
  public:
    ////////////////////////////////////////
    using AppExecType = AppExec;
    using TaskPtr = typename AppExecType::TaskPtr;
    using PoolIdType = typename AppExecType::PoolIdType;

    using timer_id_type = TimerId::IdType;
    using steady_time_point = std::chrono::steady_clock::time_point;
    using system_time_point = std::chrono::system_clock::time_point;

    Timer(AppExecType& app_e)
        : app_e_(app_e)
    {
    }

    ~Timer()
    {
        close();
    }

    TimerId scheduleTask(TaskPtr task, steady_time_point time, PoolIdType pool)
    {
        std::lock_guard<std::mutex> lock{mutex_};
        if (!steady_timer_)
            steady_timer_ = std::make_unique<SteadyTimer>(app_e_);

        return TimerId{TimerType::steady, steady_timer_->add(std::move(task), time, pool)};
    }

    TimerId scheduleTask(TaskPtr task, system_time_point time, PoolIdType pool)
    {
        std::lock_guard<std::mutex> lock{mutex_};
        if (!system_timer_)
            system_timer_ = std::make_unique<SystemTimer>(app_e_);

        return TimerId{TimerType::system, system_timer_->add(std::move(task), time, pool)};
    }

    bool cancel(const TimerId& tid)
    {
        auto id = tid.getId();
        switch (tid.getType()) {
            case TimerType::system: {
                std::lock_guard<std::mutex> lock{mutex_};
                if (system_timer_)
                    return system_timer_->cancel(id);
                break;
            }
            case TimerType::steady: {
                std::lock_guard<std::mutex> lock{mutex_};
                if (steady_timer_)
                    return steady_timer_->cancel(id);
                break;
            }
            case TimerType::unknown:
                break;
        }
        return false;
    }

    void close()
    {
        std::lock_guard<std::mutex> lock{mutex_};
        try {
            if (steady_timer_)
                steady_timer_->close();
        } catch (...) {
        }
        try {
            if (system_timer_)
                system_timer_->close();
        } catch (...) {
        }
    }

  private:
    template <TimerType type>
    struct timer_entry
    {
        using time_point = typename TimerTraits<type>::time_point;

        const time_point time_point_;
        const PoolIdType pool_;
        const timer_id_type id_;
        mutable TaskPtr task_;
    };

    template <TimerType type>
    friend bool operator<(const timer_entry<type>& tel, const timer_entry<type>& ter)
    {
        return tel.time_point_ < ter.time_point_;
    }

    template <TimerType type>
    class TimerThreadData
    {
      public:
        using time_point = typename TimerTraits<type>::time_point;

        TimerThreadData(AppExecType& app_e)
            : thread_{&TimerThreadData::loop, this, std::ref(app_e)}
        {
        }

        ~TimerThreadData()
        {
            try {
                close();
                thread_.join();
            } catch (...) {
            }
        }

        TimerId::IdType add(TaskPtr task, time_point time, PoolIdType pool)
        {
            bool notify{false};
            TimerId::IdType id{};
            {
                std::lock_guard<std::mutex> lock{mutex_};
                if (!times_.empty()) {
                    if (time < next_timeout)
                        notify = true;
                } else {
                    notify = true;
                }

                id = nextId();
                auto it = times_.insert(timer_entry<type>{time, pool, id, std::move(task)});
                it_map.insert(std::make_pair(id, timer_entry_it{id, it}));
            }

            if (notify)
                cond_.notify_one();

            return id;
        }

        bool cancel(TimerId::IdType id)
        {
            std::lock_guard<std::mutex> lock{mutex_};
            auto it = it_map.find(id);
            if (it != it_map.end()) {
                times_.erase(it->second.it);
                it_map.erase(it);
                return true;
            }
            return false;
        }

        void clear()
        {
            std::unique_lock<std::mutex> lock{mutex_};
            times_.clear();
        }

        void close()
        {
            {
                std::lock_guard<std::mutex> lock{mutex_};
                running_ = false;
            }
            cond_.notify_all();
        }

      private:
        uint64_t nextId()
        {
            id_generator++;
            if (id_generator == 0)
                id_generator = 1;
            return id_generator;
        }

        void waitForTimeout(AppExecType& app_e, std::unique_lock<std::mutex>& lock)
        {
            next_timeout = times_.begin()->time_point_;
            if (cond_.wait_until(lock, next_timeout) == std::cv_status::timeout) {
                while (running_ && !times_.empty()) {
                    auto begin = times_.begin();
                    if (begin->time_point_ == next_timeout) {
                        try {
                            TaskPtr task{std::move(begin->task_)};
                            PoolIdType pool = begin->pool_;
                            timer_id_type id = begin->id_;

                            it_map.erase(id);
                            times_.erase(begin);

                            auto res = app_e.doTask(std::move(task), pool);
                            if (res != ThreadPoolCode::queue_ok) {
                                // TODO
                            }

                        } catch (...) {
                            // TODO
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        void loop(AppExecType& app_e) noexcept
        {
            app_e.getTraits().onTimerInit(type);
            std::unique_lock<std::mutex> lock{mutex_};
            while (running_) {
                if (times_.empty()) {
                    cond_.wait(lock);
                } else {
                    waitForTimeout(app_e, lock);
                }
            }
            app_e.getTraits().onTimerDestroy(type);
        }

        using timer_entry_multiset = std::multiset<timer_entry<type>>;
        struct timer_entry_it
        {
            using iterator = typename timer_entry_multiset::iterator;
            timer_id_type id;
            iterator it;
        };
        using timer_it_map = std::unordered_map<timer_id_type, timer_entry_it>;

        /////////////////////////////////////////////////////////
        // data
        /////////////////////////////////////////////////////////
        bool running_{true};
        timer_id_type id_generator{0};
        time_point next_timeout;
        std::mutex mutex_;
        std::condition_variable cond_;
        timer_entry_multiset times_;
        timer_it_map it_map;

        std::thread thread_;
    };

    using SystemTimer = TimerThreadData<TimerType::system>;
    using SteadyTimer = TimerThreadData<TimerType::steady>;

    AppExecType& app_e_;
    std::unique_ptr<SystemTimer> system_timer_;
    std::unique_ptr<SteadyTimer> steady_timer_;
    std::mutex mutex_;
};

} /* namespace app */

} /* namespace ya */
