/**
 * GENERATED CODE FROM "ThreadPoolCode.enum" FILE DO NOT MODIFY
 */
#include "ThreadPoolCode.h"
#include <algorithm>

namespace ya {
namespace app {

static const char* const NAMES[] = {
        "queue_ok", "queue_is_empty", "queue_is_full", "queue_is_closed", "queue_not_found", nullptr};

std::array<ThreadPoolCode, 5> const& enum_list(ThreadPoolCode dummy)
{
    static const std::array<ThreadPoolCode, 5> __list{ThreadPoolCode::queue_ok,
                                                      ThreadPoolCode::queue_is_empty,
                                                      ThreadPoolCode::queue_is_full,
                                                      ThreadPoolCode::queue_is_closed,
                                                      ThreadPoolCode::queue_not_found};
    return __list;
}

static std::array<ThreadPoolCode, 5> const& sorted_enum_list(ThreadPoolCode dummy)
{
    static const std::array<ThreadPoolCode, 5> __list{ThreadPoolCode::queue_is_closed,
                                                      ThreadPoolCode::queue_is_empty,
                                                      ThreadPoolCode::queue_is_full,
                                                      ThreadPoolCode::queue_not_found,
                                                      ThreadPoolCode::queue_ok};
    return __list;
}

const char* to_cstring(ThreadPoolCode value)
{
    return NAMES[static_cast<unsigned>(value)];
}

std::string to_string(ThreadPoolCode value)
{
    return to_cstring(value);
}

std::ostream& operator<<(std::ostream& out, ThreadPoolCode value)
{
    return out << to_cstring(value);
}

namespace {
struct ThreadPoolCode_cmp
{
    bool operator()(ThreadPoolCode lhs, std::string const& rhs)
    {
        return to_cstring(lhs) < rhs;
    }
    bool operator()(std::string const& lhs, ThreadPoolCode rhs)
    {
        return lhs < to_cstring(rhs);
    }
};
}

std::istream& operator>>(std::istream& in, ThreadPoolCode& value)
{
    std::string s;
    if (in >> s) {
        ThreadPoolCode_cmp cmp;
        auto const& sorted = sorted_enum_list(ThreadPoolCode{});
        auto it = std::lower_bound(sorted.begin(), sorted.end(), s, cmp);
        if ((it != sorted.end()) && (to_cstring(*it) == s)) {
            value = *it;
            return in;
        }
    }
    in.clear(std::ios::failbit);
    return in;
}

} /* namespace app */
} /* namespace ya */
