/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include <chrono>
#include <condition_variable>
#include <cstring>
#include <future>
#include <mutex>
#include <queue>
#include <set>
#include <string>
#include <thread>
#include <type_traits>
#include <utility>
#include <vector>

#include <ya/utils/blocking_queue.h>
#include <ya/utils/log.h>
#include <ya/utils/string.h>

#include "ThreadPool.h"
#include "Timer.h"

namespace ya {
namespace app {

/**
 * \class AppExec
 * \brief AppExec is a class which contains execution contexts (Named Thread
 * Pools and Timer) for application.
 */
template <class APPEXECTRAIT>
class AppExec
{
  public:
    /**
     * \class Task
     * \brief Task base class.
     */
    class Task
    {
      public:
        /**
         */
        virtual ~Task() = default;
        /**
         * \brief exec execution entry point for task.
         * \fn exec
         * \param appExec a reference to AppExec
         * \param self a shared pointer to the task itself; may be used to
         * reschedule the same task in loop
         */
        virtual void exec(AppExec& appExec, const std::shared_ptr<Task>& self) noexcept = 0;
        /**
         * \brief cancel this function is called on task to abort its execution
         * of
         * to notify the task that it wont be
         * executed at all.
         * \fn cancel
         */
        virtual void cancel() noexcept
        {
        }
    };

    /// task pointer
    using TaskPtr = std::shared_ptr<Task>;

    /**
     * \brief Callable adapters
     * \{
     */
    template <class Callable>
    class Task2 : public Task
    {
      private:
        Callable call;

      public:
        Task2(Callable&& c)
            : call(std::move(c))
        {
        }
        Task2(const Callable& c)
            : call(c)
        {
        }
        void exec(AppExec& app, const TaskPtr& self) noexcept
        {
            try {
                call(app, self);
            } catch (...) {
                app.getTraits().onTaskException();
            }
        }
    };

    template <class Callable>
    class Task1 : public Task
    {
      private:
        Callable call;

      public:
        Task1(Callable&& c)
            : call(std::move(c))
        {
        }
        Task1(const Callable& c)
            : call(c)
        {
        }
        void exec(AppExec& app, const TaskPtr& self) noexcept
        {
            try {
                call(app);
            } catch (...) {
                app.getTraits().onTaskException();
            }
        }
    };

    template <class Callable>
    class Task0 : public Task
    {
      private:
        Callable call;

      public:
        Task0(Callable&& c)
            : call(std::move(c))
        {
        }
        Task0(const Callable& c)
            : call(c)
        {
        }
        void exec(AppExec& app, const TaskPtr& self) noexcept
        {
            try {
                call();
            } catch (...) {
                app.getTraits().onTaskException();
            }
        }
    };
    /**
     * \}
     */

    /*
     * types definitions
     */
    /// thread pool configuration class
    using AppExecTrait = APPEXECTRAIT;
    /// user's application main class
    using AppType = typename AppExecTrait::AppType;

    /// threads pool identifier type
    using PoolIdType = typename AppExecTrait::PoolIdType;

    /// self type
    using AppExecType = AppExec<AppExecTrait>;

    AppExec(AppType* _app = nullptr, const AppExecTrait& _traits = {})
        : traits_(_traits)
        , app_(_app)
        , timer_(*this)
    {
        for (auto pool_id : traits_.init())
            findOrCreate(pool_id);
    }

    virtual ~AppExec()
    {
        closeAll();
        cancelAll();
        wait();
    }

    void closeTimers()
    {
        timer_.close();
    }

    void closeAll()
    {
        closeTimers();
        close();
    }

    void close()
    {
        auto l = lock();
        _closeAll();
    }

    void close(std::initializer_list<PoolIdType> pools)
    {
        auto l = lock();
        for (auto p : pools) {
            auto pool = find(p);
            if (pool) {
                pool->close();
            }
        }
    }

    void cancelPending()
    {
        auto pools = getAllPools();
        for (auto pool : pools) {
            pool->cancelPending();
        }
    }

    void cancelPending(std::initializer_list<PoolIdType> const& list)
    {
        auto pools = getPools(list);
        for (auto pool : pools) {
            pool->cancelPending();
        }
    }

    void cancelAll()
    {
        auto pools = getAllPools();
        for (auto pool : pools) {
            pool->cancelAll();
        }
    }

    void cancelAll(std::initializer_list<PoolIdType> const& list)
    {
        auto pools = getPools(list);
        for (auto pool : pools) {
            pool->cancelAll();
        }
    }

    void wait()
    {
        auto pools = getAllPools();
        for (auto pool : pools) {
            pool->wait();
        }
    }

    void wait(std::initializer_list<PoolIdType> const& list)
    {
        auto pools = getPools(list);
        for (auto pool : pools) {
            pool->wait();
        }
    }

    template <class Clock, class Duration>
    void waitUntil(const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        auto pools = getAllPools();
        for (auto pool : pools) {
            pool->waitUntil(timeout_time);
        }
    }

    template <class Clock, class Duration>
    void waitUntil(const std::chrono::time_point<Clock, Duration>& timeout_time,
                   std::initializer_list<PoolIdType> const& list)
    {

        auto pools = getPools(list);
        for (auto pool : pools) {
            pool->waitUntil(timeout_time);
        }
    }

    template <class Rep, class Period>
    void waitFor(const std::chrono::duration<Rep, Period>& rel_time)
    {
        auto timeout_time = std::chrono::steady_clock::now() + rel_time;
        waitUntil(timeout_time);
    }

    template <class Rep, class Period>
    void waitFor(const std::chrono::duration<Rep, Period>& rel_time, std::initializer_list<PoolIdType> const& list)
    {

        auto timeout_time = std::chrono::steady_clock::now() + rel_time;
        waitUntil(timeout_time, list);
    }

    AppExecTrait& getTraits()
    {
        return traits_;
    }

    const AppExecTrait& getTraits() const
    {
        return traits_;
    }

    template <class C>
    TaskPtr makeTask2(C&& call)
    {
        return std::make_shared<Task2<typename std::remove_reference<C>::type>>(std::forward<C>(call));
    }

    template <class C>
    TaskPtr makeTask1(C&& call)
    {
        return std::make_shared<Task1<typename std::remove_reference<C>::type>>(std::forward<C>(call));
    }

    template <class C>
    TaskPtr makeTask0(C&& call)
    {
        return std::make_shared<Task0<typename std::remove_reference<C>::type>>(std::forward<C>(call));
    }

    template <class C>
    using Result2 = std::result_of_t<C(AppExec&, const TaskPtr&)>;
    template <class C>
    using Result1 = std::result_of_t<C(AppExec&)>;
    template <class C>
    using Result0 = std::result_of_t<C()>;

    template <class C>
    using Callable2 = Result2<C>(AppExec&, const TaskPtr&);
    template <class C>
    using Callable1 = Result1<C>(AppExec&);
    template <class C>
    using Callable0 = Result0<C>();

    ///@{
    /** Do asynchroniusly task as std::packaged_task.
        doPackagedTask takes a callable parameter and returns std::future<T>
       with
       type T deduced from a callable return
       type.  */

    /**
     * \brief doPackagedTask
     *
     * \param call a callable to call asynchronusly with signature T(AppExec&,
     *const TaskPtr&)
     * \param pool a thread pool id where the task must be executed.
     * \return std::future<T>
     */
    template <class C>
    std::future<Result2<C>> doPackagedTask(C&& call, PoolIdType pool)
    {
        std::packaged_task<Callable2<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        doTask(makeTask2(std::move(ptask)), pool);
        return future;
    }
    /**
     * \brief doPackagedTask
     *
     * \param call a callable to call asynchronusly with signature T(AppExec&)
     * \param pool a thread pool id where the task must be executed.
     * \return std::future<T>
     */
    template <class C>
    std::future<Result1<C>> doPackagedTask(C&& call, PoolIdType pool)
    {
        std::packaged_task<Callable1<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        doTask(makeTask1(std::move(ptask)), pool);
        return future;
    }
    /**
     * \brief doPackagedTask
     *
     * \param call a callable to call asynchronusly with signature T()
     * \param pool a thread pool id where the task must be executed.
     * \return std::future<T>
     */
    template <class C>
    std::future<Result0<C>> doPackagedTask(C&& call, PoolIdType pool)
    {
        std::packaged_task<Callable0<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        doTask(makeTask0(std::move(ptask)), pool);
        return future;
    }
    /// @}

    template <class C, class T>
    std::pair<std::future<Result2<C>>, TimerId> schedulePackagedTask(C&& call, PoolIdType pool, T t)
    {
        std::packaged_task<Callable2<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        auto timerId = scheduleTask(makeTask2(std::move(ptask)), pool, t);
        return std::make_pair(std::move(future), timerId);
    }

    template <class C, class T>
    std::pair<std::future<Result1<C>>, TimerId> schedulePackagedTask(C&& call, PoolIdType pool, T t)
    {
        std::packaged_task<Callable1<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        auto timerId = scheduleTask(makeTask1(std::move(ptask)), pool, t);
        return std::make_pair(std::move(future), timerId);
    }

    template <class C, class T>
    std::pair<std::future<Result0<C>>, TimerId> schedulePackagedTask(C&& call, PoolIdType pool, T t)
    {
        std::packaged_task<Callable0<C>> ptask{std::forward<C>(call)};

        auto future = ptask.get_future();
        auto timerId = scheduleTask(makeTask0(std::move(ptask)), pool, t);
        return std::make_pair(std::move(future), timerId);
    }

    /*
     * Support for any callable
     *
     */

    template <class T>
    struct void_type
    {
    };

    template <class C>
    ThreadPoolCode doSimpleTask(C&& call, PoolIdType pool, void_type<Result2<C>> _void = {})
    {
        return doTask(makeTask2(std::forward<C>(call)), pool);
    }

    template <class C>
    ThreadPoolCode doSimpleTask(C&& call, PoolIdType pool, void_type<Result1<C>> _void = {})
    {
        return doTask(makeTask1(std::forward<C>(call)), pool);
    }

    template <class C>
    ThreadPoolCode doSimpleTask(C&& call, PoolIdType pool, void_type<Result0<C>> _void = {})
    {
        return doTask(makeTask0(std::forward<C>(call)), pool);
    }

    template <class C, class T>
    TimerId scheduleSimpleTask(C&& call, PoolIdType pool, T t, void_type<Result2<C>> _void = {})
    {
        return scheduleTask(makeTask2(std::forward<C>(call)), pool, t);
    }

    template <class C, class T>
    TimerId scheduleSimpleTask(C&& call, PoolIdType pool, T t, void_type<Result1<C>> _void = {})
    {
        return scheduleTask(makeTask1(std::forward<C>(call)), pool, t);
    }

    template <class C, class T>
    TimerId scheduleSimpleTask(C&& call, PoolIdType pool, T t, void_type<Result0<C>> _void = {})
    {
        return scheduleTask(makeTask0(std::forward<C>(call)), pool, t);
    }

    /**
     * \brief doTask enques task on pool-queue
     * \param task a task
     * \param pool_id a pool id
     */
    ThreadPoolCode doTask(const TaskPtr& task, PoolIdType pool_id)
    {
        ThreadPoolType* tp = nullptr;
        {
            std::lock_guard<std::mutex> lock{mutex_};
            tp = findOrCreate(pool_id);
        }
        if (tp) {
            return tp->push(task);
        }
        return ThreadPoolCode::queue_is_closed;
    }

    TimerId scheduleTask(const TaskPtr& task, PoolIdType pool, const std::chrono::steady_clock::duration& dur)
    {
        return timer_.scheduleTask(task, std::chrono::steady_clock::now() + dur, pool);
    }

    TimerId scheduleTask(const TaskPtr& task, PoolIdType pool, const std::chrono::steady_clock::time_point& tp)
    {
        return timer_.scheduleTask(task, tp, pool);
    }

    TimerId scheduleTask(const TaskPtr& task, PoolIdType pool, const std::chrono::system_clock::time_point& tp)
    {
        return timer_.scheduleTask(task, tp, pool);
    }

    bool cancel(const TimerId& id)
    {
        return timer_.cancel(id);
    }

    AppType* app() const
    {
        return app_;
    }

  private:
    using ThreadPoolType = ThreadPool<AppExecType>;

    std::mutex mutex_;
    AppExecTrait traits_;
    bool running_{true};
    AppType* app_;
    Timer<AppExecType> timer_;

    std::map<PoolIdType, ThreadPoolType> pools_;

    void _closeAll()
    {
        running_ = false;
        for (auto& tpe : pools_)
            tpe.second.close();
    }

    ThreadPoolType* find(PoolIdType pool)
    {
        auto it = pools_.find(pool);
        if (it != pools_.end()) {
            return &it->second;
        }
        return nullptr;
    }

    ThreadPoolType* findOrCreate(PoolIdType pool)
    {
        auto p = find(pool);
        if ((p == nullptr) && running_) {
            auto res = pools_.emplace(
                    std::piecewise_construct, std::forward_as_tuple(pool), std::forward_as_tuple(pool, *this));
            return &res.first->second;
        }
        return p;
    }

    std::unique_lock<std::mutex> lock()
    {
        return std::unique_lock<std::mutex>{mutex_};
    }

    std::vector<ThreadPoolType*> getAllPools()
    {
        std::vector<ThreadPoolType*> pools;
        auto l = lock();
        pools.reserve(pools_.size());
        for (auto& tpe : pools_) {
            pools.push_back(&tpe.second);
        }
        return pools;
    }

    std::vector<ThreadPoolType*> getPools(std::initializer_list<PoolIdType> const& list)
    {
        std::vector<ThreadPoolType*> pools;
        auto l = lock();
        pools.reserve(pools_.size());
        for (auto pool : list) {
            auto p = find(pool);
            if (p)
                pools.push_back(p);
        }
        return pools;
    }
};

template <class PoolId, class App>
struct AppExecTrait
{

    using PoolIdType = PoolId;
    using AppType = App;

    virtual std::initializer_list<PoolIdType> init() const noexcept = 0;
    virtual unsigned number(PoolId id) const noexcept = 0;
    virtual std::size_t maxQueueSize(PoolId id) const noexcept = 0;

    virtual void onThreadInit(PoolId id, unsigned no) noexcept = 0;
    virtual void onThreadDestroy(PoolId id, unsigned no) noexcept = 0;
    virtual void onTimerInit(TimerId::Type type) noexcept = 0;
    virtual void onTimerDestroy(TimerId::Type type) noexcept = 0;
    virtual void onTaskException() noexcept = 0;
};
}
}
