#include <memory>

namespace ya {
namespace std {

template <class T, class... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>{new T{std::forward<Args>(args)...}};
}

} /* namespace std */
} /* namespace ya */
