namespace ya {
namespace std {

template <class CharT, Traits = std::char_traits<CharT>>
class basic_string_view
{
  public:
    using type_traits = Traits;
    using value_type = CharT;
    using pointer = value_type*;
    using const_pointer = value_type const*;
    using reference = value_type&;
    using const_reference = value_type const&;
    using const_iterator = pointer;
    using iterator = const_iterator;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    constexpr basic_string_view() noexcept
    {
    }

    constexpr basic_string_view(basic_string_view const& other) noexcept = default;

    constexpr basic_string_view(const_pointer s, size_type size)
        : str{s}
        , size{size}
    {
    }

    constexpr basic_string_view(const_pointer s)
        : str{s}
        , size{Traits::length(s)}
    {
    }

  private:
    const_pointer* str = nullptr;
    size_type size = 0;
};

} /* namespace std */
} /* namespace ya */
