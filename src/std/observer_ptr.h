/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include <memory>
#include <type_traits>
#include <utility>

#include "type_traits.h"

namespace ya {

namespace std {

template <class T>
class observer_ptr
{
  public:
    using element_type = typename std::decay<T>::type;

    constexpr observer_ptr() noexcept : ptr{nullptr}
    {
    }

    constexpr observer_ptr(nullptr_t) noexcept : ptr{nullptr}
    {
    }

    constexpr observer_ptr(element_type* p) noexcept : ptr(p)
    {
    }

    template <class W2>
    constexpr observer_ptr(observer_ptr<W2> other) noexcept : ptr{other.get()}
    {
    }

    observer_ptr(observer_ptr<T> const&) = default;
    observer_ptr(observer_ptr<T>&&) = default;

    constexpr void reset(element_type* p = nullptr) noexcept
    {
        ptr = p;
    }

    constexpr element_type* release() noexcept
    {
        element_type* tmp = ptr;
        ptr = nullptr;
        return tmp;
    }

    constexpr void swap(observer_ptr<T>& other) noexcept
    {
        std::swap(ptr, other.ptr);
    }

    element_type* get() const noexcept
    {
        return ptr;
    }

    operator bool() const noexcept
    {
        return ptr != nullptr;
    }

    constexpr std::add_lvalue_reference_t<element_type> operator*() const noexcept
    {
        return *get();
    }

    constexpr element_type* operator->() const noexcept
    {
        return get();
    }

    constexpr explicit operator element_type*() const noexcept
    {
        return *get();
    }

  private:
    element_type* ptr;
};

template <class W>
observer_ptr<W> make_observer(W* p)
{
    return observer_ptr<W>{p};
}

template <class W1, class W2>
bool operator==(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2)
{
    return p1.get() == p2.get();
}

template <class W1, class W2>
bool operator!=(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2)
{
    return !(p1 == p2);
}

template <class W>
bool operator==(const observer_ptr<W>& p, std::nullptr_t) noexcept
{
    return !p;
}

template <class W>
bool operator==(std::nullptr_t, const observer_ptr<W>& p) noexcept
{
    return !p;
}

template <class W>
bool operator!=(const observer_ptr<W>& p, std::nullptr_t) noexcept
{
    return p;
}

template <class W>
bool operator!=(std::nullptr_t, const observer_ptr<W>& p) noexcept
{
    return p;
}
#if 0
template <class W1, class W2>
bool operator<(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2) {
    return false;
}
template <class W1, class W2>
bool operator>(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2) {
    return false;
}
template <class W1, class W2>
bool operator<=(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2) {
    return false;
}
template <class W1, class W2>
bool operator>=(const observer_ptr<W1>& p1, const observer_ptr<W2>& p2) {
    return false;
}
#endif

} /* namespace std */
} /* namespace ya */
