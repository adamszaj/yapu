/**
 * GENERATED CODE FROM "ExampleProgramOptions.enum" FILE DO NOT MODIFY
 */
#include "ExampleProgramOptions.h"
#include <algorithm>

namespace ya {
namespace example {

namespace {
struct ExampleProgramOptions_cmp
{
    bool operator()(ExampleProgramOptions lhs, std::string const& rhs)
    {
        return to_cstring(lhs) < rhs;
    }
    bool operator()(std::string const& lhs, ExampleProgramOptions rhs)
    {
        return lhs < to_cstring(rhs);
    }
};
const char* const names[] = {"foo", "bar", "baz", nullptr};
}

std::array<ExampleProgramOptions, 3> const& enum_list(ExampleProgramOptions dummy)
{
    static const std::array<ExampleProgramOptions, 3> __list{
            ExampleProgramOptions::foo, ExampleProgramOptions::bar, ExampleProgramOptions::baz};
    return __list;
}

static std::array<ExampleProgramOptions, 3> const& sorted_enum_list(ExampleProgramOptions dummy)
{
    static const std::array<ExampleProgramOptions, 3> __list{
            ExampleProgramOptions::bar, ExampleProgramOptions::baz, ExampleProgramOptions::foo};
    return __list;
}

const char* to_cstring(ExampleProgramOptions value)
{
    return names[static_cast<unsigned>(value)];
}

std::string to_string(ExampleProgramOptions value)
{
    return to_cstring(value);
}

std::ostream& operator<<(std::ostream& out, ExampleProgramOptions value)
{
    return out << to_cstring(value);
}

std::istream& operator>>(std::istream& in, ExampleProgramOptions& value)
{
    std::string s;
    if (in >> s) {
        ExampleProgramOptions_cmp cmp;
        auto const& sorted = sorted_enum_list(ExampleProgramOptions{});
        auto it = std::lower_bound(sorted.begin(), sorted.end(), s, cmp);
        if ((it != sorted.end()) && (to_cstring(*it) == s)) {
            value = *it;
            return in;
        }
    }
    in.clear(std::ios::failbit);
    return in;
}

} /* namespace example */
} /* namespace ya */
