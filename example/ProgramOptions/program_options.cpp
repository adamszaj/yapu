#include <iomanip>

#include <ya/app/options/ProgramOptions.h>
#include <ya/utils/log.h>

#include "ExampleProgramOptions.h"

namespace opt = ya::app::options;

int main(int argc, char* argv[]) try {
    std::vector<std::string> args{argv, argv + argc};

    ya::utils::set_logger(args[0], "stderr:");
    ya::utils::set_thread_name("main");
    ya::utils::set_log_level(LOG_DEBUG);

    opt::ProgramOptions opts;

    std::string s;
    std::string name;
    std::vector<int> iv;
    std::vector<std::string> positionals;
    int i{};

    bool enable_1{};
    bool enable_2{};
    bool enable_3{};

    using ya::example::ExampleProgramOptionsFlags;
    using ya::example::ExampleProgramOptions;
    ya::example::ExampleProgramOptionsFlags flags;

    opts.addOption("str,s", opt::make_param_option(s), opt::Description("string value"))
            .addOption("v,v", opt::make_param_option(iv))
            .addOption("e1,1", opt::make_option(enable_1, true))
            .addOption("e2,2", opt::make_option(enable_2, false))
            .addOption("e3,3", opt::make_option(enable_3, true))
            .addOption("int,i", opt::make_param_option(i))
            .addOptions(flags, "no-")
            /// any positional except those explicitly assigned
            .addPositional(opt::make_param_option(positionals), opt::ProgramOptions::position_any{})
            /// first positional
            .addPositional(opt::make_param_option(name));

    opts.parse(args);

    log_info("opts:\n" << opts);

    log_info("s: '" << s << "' i: " << i << "\n");

    log_info("enable_1: " << enable_1);
    log_info("enable_2: " << enable_2);
    log_info("enable_3: " << enable_3);

    for (auto f : enum_list(ExampleProgramOptions{})) {
        log_info("option: " << f << " : " << flags.test(f));
    }

    for (auto i : iv) {
        log_info("i: " << i);
    }

    log_info("name: " << name);

    for (auto i : positionals) {
        log_info("positionals: " << i);
    }

    return 0;
} /* end main() */
catch (const std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
} catch (...) {
    std::cerr << "unknown exception\n";
}
