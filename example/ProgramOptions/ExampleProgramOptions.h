/**
 * GENERATED CODE FROM "ExampleProgramOptions.enum" FILE DO NOT MODIFY
 */
#pragma once
#include <string>
#include <iostream>
#include <array>

#include <ya/utils/flags.h>

namespace ya {
namespace example {

enum class ExampleProgramOptions
{
    foo,
    bar,
    baz
};

const char* to_cstring(ExampleProgramOptions value);
std::string to_string(ExampleProgramOptions value);
std::ostream& operator<<(std::ostream& out, ExampleProgramOptions value);
std::istream& operator>>(std::istream& out, ExampleProgramOptions& value);
std::array<ExampleProgramOptions, 3> const& enum_list(ExampleProgramOptions dummy);

using ExampleProgramOptionsFlags = ya::utils::flags<ExampleProgramOptions, 3>;

} /* namespace example */
} /* namespace ya */
