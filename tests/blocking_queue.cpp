#include <future>
#include <gtest/gtest.h>
#include <string>
#include <thread>
#include <ya/utils/blocking_queue.h>

using ya::utils::blocking_queue;
using ya::utils::queue_code;
using ya::utils::queue_block;

using IBQ = blocking_queue<int>;
#define False 0
#define Talse 1
TEST(blocking_queue, test_empty)
{
    IBQ ibq;

    EXPECT_EQ(Talse, ibq.empty());
    EXPECT_EQ(queue_code::queue_ok, ibq.push(1));
    EXPECT_EQ(False, ibq.empty());
    EXPECT_EQ(1, ibq.pop());
    EXPECT_EQ(Talse, ibq.empty());
}

TEST(blocking_queue, test_closed)
{
    IBQ ibq;
    int i;

    EXPECT_EQ(False, ibq.closed());
    EXPECT_EQ(queue_code::queue_ok, ibq.push(1));
    EXPECT_EQ(queue_code::queue_ok, ibq.push(2));
    EXPECT_EQ(queue_code::queue_ok, ibq.push(3));
    EXPECT_EQ(queue_code::queue_ok, ibq.push(4));

    EXPECT_EQ(False, ibq.closed());

    EXPECT_EQ(1, ibq.pop());
    EXPECT_EQ(2, ibq.pop());
    EXPECT_EQ(False, ibq.closed());
    ibq.close();
    EXPECT_EQ(Talse, ibq.closed());
    EXPECT_EQ(3, ibq.pop());
    EXPECT_EQ(4, ibq.pop());
    EXPECT_EQ(queue_code::queue_is_closed, ibq.pop(i, queue_block{False}));
    EXPECT_EQ(queue_code::queue_is_closed, ibq.pop(i));
}

TEST(blocking_queue, test_size_limit)
{
    IBQ ibq{5};
    EXPECT_EQ(1, ibq.empty());
    ibq.push(1);
    ibq.push(2);
    ibq.push(3);
    ibq.push(4);
    ibq.push(5);
    EXPECT_EQ(queue_code::queue_is_full, ibq.push(6, queue_block{False}));
    ibq.close();
    int i;
    EXPECT_EQ(queue_code::queue_is_closed, ibq.push(6, queue_block{False}));
    EXPECT_EQ(queue_code::queue_ok, ibq.pop(i));
    EXPECT_EQ(queue_code::queue_ok, ibq.pop(i));
    EXPECT_EQ(queue_code::queue_ok, ibq.pop(i));
    EXPECT_EQ(queue_code::queue_ok, ibq.pop(i));
    EXPECT_EQ(0, ibq.empty());
    EXPECT_EQ(queue_code::queue_ok, ibq.pop(i));
    EXPECT_EQ(1, ibq.empty());
    EXPECT_EQ(1, ibq.closed());
    EXPECT_EQ(queue_code::queue_is_closed, ibq.pop(i));
}

queue_code readIBQ(IBQ& ibq)
{
    int i;
    queue_code code;
    while ((code = ibq.pop(i)) == queue_code::queue_ok)
        ;
    return code;
}

TEST(blocking_queue, test_notify_close)
{
    IBQ ibq;

    std::vector<std::future<queue_code>> vf(5);
    for (auto& f : vf) {
        f = std::async(std::launch::async, readIBQ, std::ref(ibq));
    }

    ibq.close();

    for (auto& f : vf) {
        EXPECT_EQ(queue_code::queue_is_closed, f.get());
    }
}

TEST(blocking_queue, test_push_block)
{
    IBQ ibq(5);
    auto f = std::async(std::launch::async, [&ibq]() -> int {
        int i = 0;
        for (; i < 10; ++i) {
            ibq.push(i);
        }
        ibq.close();
        return i;
    });

    for (int i = 0; i < 5; ++i) {
        EXPECT_EQ(i, ibq.pop());
    }
    EXPECT_EQ(10, f.get());
}

TEST(blocking_queue, test_pop_2)
{
    blocking_queue<std::string> sbq;
    for (int i = 0; i < 20; i++)
        sbq.push(std::to_string(i) + "_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

    for (int i = 0; i < 20; i++) {
        EXPECT_EQ(std::to_string(i) + "_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", sbq.pop());
    }
}
