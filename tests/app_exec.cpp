/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include "thname.h"
#include <algorithm>
#include <gtest/gtest.h>
#include <iomanip>
#include <iostream>
#include <unistd.h>
#include <ya/app/AppExec.h>
#include <ya/utils/lock.h>
#include <ya/utils/log.h>

namespace test {

using namespace std;

using ya::app::AppExec;
using ya::app::TimerType;

class ThreadTraits : public ya::app::AppExecTrait<thname, void>
{
  private:
    std::atomic<unsigned> exception_counter;

  public:
    using PoolIdType = thname;

    ThreadTraits()
        : exception_counter{}
    {
    }

    ThreadTraits(const ThreadTraits& tt)
        : exception_counter{tt.exception_counter.load()}
    {
    }

    std::initializer_list<PoolIdType> init() const noexcept override
    {
        return {};
    }

    std::size_t maxQueueSize(thname id) const noexcept override
    {
        return 0;
    }

    unsigned number(thname id) const noexcept override
    {
        switch (id) {
            case thname::exec_default:
                return 2;
            case thname::exec_one:
                return 1;
            case thname::exec_zero:
                return 0;
            case thname::exec_timer:
                return 1;
        }
        return 0;
    }

    void onThreadInit(thname id, unsigned no) noexcept override
    {
        std::ostringstream o;
        o << "worker[" << id << ":" << no << "]";
        ya::utils::set_thread_name(o.str());
        log_notice("create thread: " << id << ':' << no);
    }

    void onThreadDestroy(thname id, unsigned no) noexcept override
    {
        log_notice("destroy thread: " << id << ':' << no);
    }

    void onTimerInit(TimerType timer) noexcept override
    {
        std::ostringstream o;
        o << "timer[" << timer << "]";
        ya::utils::set_thread_name(o.str());
        log_notice("create thread: " << o.str());
    }

    void onTimerDestroy(TimerType timer) noexcept override
    {
        log_notice("destroy thread: timer[" << timer << "]");
    }

    void onTaskException() noexcept override
    {
        try {
            exception_counter++;
            throw;
        } catch (std::exception const& e) {
            log_error("exception from task: " << e.what());
        } catch (...) {
            log_error("unknown exception");
        }
    }
    unsigned getExceptionCounter() const
    {
        return exception_counter;
    }
};
}

using TestAppExec = ya::app::AppExec<test::ThreadTraits>;
using test::thname;

class ShortTask : public TestAppExec::Task
{
    std::atomic<unsigned> counter{0};

  public:
    void exec(TestAppExec& appExec, const TestAppExec::TaskPtr& self) noexcept override;
    unsigned getCounter() const
    {
        return counter;
    }
};

void ShortTask::exec(TestAppExec& appExec, const TestAppExec::TaskPtr& self) noexcept
{
    counter++;
}

using ya::utils::spin_lock;

class LongTask : public TestAppExec::Task
{
  private:
    unsigned n_of_10ms;
    unsigned counter{0};
    bool cancelled{false};
    bool done{false};
    mutable spin_lock mutex;

  public:
    LongTask(unsigned _n_of_10ms = 100)
        : n_of_10ms{_n_of_10ms}
    {
    }

    void exec(TestAppExec& appExec, const TestAppExec::TaskPtr& self) noexcept override
    {
        std::unique_lock<spin_lock> lock{mutex};
        if (!cancelled) {
            counter++;
            for (unsigned i = 0; !cancelled && i < n_of_10ms; ++i) {
                lock.unlock();
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
                lock.lock();
            }
            if (!cancelled)
                done = true;
        }
    }

    void cancel() noexcept override
    {
        std::lock_guard<spin_lock> lock{mutex};
        if (!done)
            cancelled = true;
    }

    unsigned getCounter() const
    {
        std::lock_guard<spin_lock> lock{mutex};
        return counter;
    }
    bool getCancelled() const
    {
        std::lock_guard<spin_lock> lock{mutex};
        return cancelled;
    }
    bool getDone() const
    {
        std::lock_guard<spin_lock> lock{mutex};
        return done;
    }
};

static void simpleTask0()
{
}
static void simpleTask1(TestAppExec& appExec)
{
}
static void simpleTask2(TestAppExec& appExec, const TestAppExec::TaskPtr& self)
{
}

TEST(AppExec, empty)
{
    ASSERT_NO_THROW(TestAppExec());
}

TEST(AppExec, API)
{
    TestAppExec appExec;

    auto rel_time = std::chrono::milliseconds(10);
    auto abs_sys_time = std::chrono::system_clock::now() + rel_time;
    auto abs_ste_time = std::chrono::steady_clock::now() + rel_time;

    auto task = std::make_shared<ShortTask>();
    auto exec_default = thname::exec_default;

    appExec.doTask(task, exec_default);
    appExec.scheduleTask(task, exec_default, rel_time);
    appExec.scheduleTask(task, exec_default, abs_sys_time);
    appExec.scheduleTask(task, exec_default, abs_ste_time);

    appExec.doSimpleTask(&simpleTask0, exec_default);
    appExec.doSimpleTask(&simpleTask1, exec_default);
    appExec.doSimpleTask(&simpleTask2, exec_default);

    std::vector<std::future<void>> packResults;

    packResults.emplace_back(appExec.doPackagedTask(&simpleTask0, exec_default));
    packResults.emplace_back(appExec.doPackagedTask(&simpleTask1, exec_default));
    packResults.emplace_back(appExec.doPackagedTask(&simpleTask2, exec_default));

    appExec.scheduleSimpleTask(&simpleTask0, exec_default, rel_time);
    appExec.scheduleSimpleTask(&simpleTask1, exec_default, rel_time);
    appExec.scheduleSimpleTask(&simpleTask2, exec_default, rel_time);

    appExec.scheduleSimpleTask(&simpleTask0, exec_default, abs_sys_time);
    appExec.scheduleSimpleTask(&simpleTask1, exec_default, abs_sys_time);
    appExec.scheduleSimpleTask(&simpleTask2, exec_default, abs_sys_time);

    appExec.scheduleSimpleTask(&simpleTask0, exec_default, abs_ste_time);
    appExec.scheduleSimpleTask(&simpleTask1, exec_default, abs_ste_time);
    appExec.scheduleSimpleTask(&simpleTask2, exec_default, abs_ste_time);

    // return std::pair<std::future<R>, TimerId>
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask0, exec_default, rel_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask1, exec_default, rel_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask2, exec_default, rel_time);
        packResults.emplace_back(std::move(fi.first));
    }

    {
        auto fi = appExec.schedulePackagedTask(&simpleTask0, exec_default, abs_sys_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask1, exec_default, abs_sys_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask2, exec_default, abs_sys_time);
        packResults.emplace_back(std::move(fi.first));
    }

    {
        auto fi = appExec.schedulePackagedTask(&simpleTask0, exec_default, abs_ste_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask1, exec_default, abs_ste_time);
        packResults.emplace_back(std::move(fi.first));
    }
    {
        auto fi = appExec.schedulePackagedTask(&simpleTask2, exec_default, abs_ste_time);
        packResults.emplace_back(std::move(fi.first));
    }

    for (auto& f : packResults) {
        ASSERT_NO_THROW(f.get());
    }
}

TEST(AppExec, runSomeShortTasks)
{
    TestAppExec appExec;
    auto task = std::make_shared<ShortTask>();
    for (unsigned i = 0; i < 1000; i++) {
        appExec.doTask(task, thname::exec_default);
    }
    appExec.closeAll();
    appExec.cancelAll();
    EXPECT_GE(1000, task->getCounter());
}

TEST(AppExec, runSomeShortTasksAndWait)
{
    TestAppExec appExec;
    auto task = std::make_shared<ShortTask>();
    for (unsigned i = 0; i < 1000; i++) {
        appExec.doTask(task, thname::exec_default);
    }
    appExec.closeAll();
    appExec.wait();
    EXPECT_EQ(1000, task->getCounter());
}

TEST(AppExec, runSomeLongTasksAndWaitFor)
{
    TestAppExec appExec;
    std::vector<std::shared_ptr<LongTask>> tasks(100); // 100 for 1 sec
    for (auto& t : tasks)
        t = std::make_shared<LongTask>();

    for (unsigned i = 0; i < 10; i++)
        appExec.doTask(tasks[i], thname::exec_default);

    appExec.closeAll();
    appExec.waitFor(std::chrono::milliseconds(100));
    appExec.cancelAll();
    appExec.wait();

    EXPECT_EQ(10, std::count_if(tasks.begin(), tasks.end(), [](const std::shared_ptr<LongTask>& task) {
                  return task->getCancelled() != task->getDone();
              }));
}

TEST(AppExec, runSomeLongTasksAndWaitUntil)
{
    TestAppExec appExec;
    std::vector<std::shared_ptr<LongTask>> tasks(100); // 100 for 1 sec
    for (auto& t : tasks)
        t = std::make_shared<LongTask>();

    for (unsigned i = 0; i < 10; i++)
        appExec.doTask(tasks[i], thname::exec_default);

    auto timeout = std::chrono::steady_clock::now() + std::chrono::milliseconds(100);
    appExec.closeAll();
    appExec.waitUntil(timeout);
    appExec.cancelAll();
    appExec.wait();

    EXPECT_EQ(10, std::count_if(tasks.begin(), tasks.end(), [](const std::shared_ptr<LongTask>& task) {
                  return task->getCancelled() != task->getDone();
              }));
}

TEST(AppExec, exceptionInTask)
{
    TestAppExec appExec;
    appExec.doSimpleTask([]() { throw std::logic_error("something failed"); }, thname::exec_default);
    appExec.close();
    appExec.wait();
    EXPECT_EQ(1, appExec.getTraits().getExceptionCounter());
}

TEST(AppExec, timerTestSimple)
{
    TestAppExec appExec;
    int counter = 10;
    appExec.scheduleSimpleTask(
            [&counter](TestAppExec& appExec, TestAppExec::TaskPtr const& self) {
                if (counter) {
                    counter--;
                    appExec.scheduleTask(self, thname::exec_default, std::chrono::milliseconds(100));
                } else {
                    appExec.close();
                }
            },
            thname::exec_default,
            std::chrono::milliseconds(100));
    sleep(2);
    EXPECT_EQ(0, counter);
}
