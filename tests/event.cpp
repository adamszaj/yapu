/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include <ya/app/EventDispatcher.h>
#include <ya/utils/log.h>

#include <tuple>
#include <type_traits>

namespace test {

class TestListener
{
  protected:
    virtual ~TestListener() = default;

  public:
    virtual void onIntData(int v, int x) noexcept = 0;
    virtual void onStringData(const std::string& s) noexcept = 0;
    virtual void onStringVectorData(const std::vector<std::string>& vs) noexcept = 0;
};

class TestListenerImpl : public TestListener
{
  public:
    void onIntData(int v, int x) noexcept
    {
        log_info("value: " << v << " x: " << x);
    }
    void onStringData(const std::string& s) noexcept
    {
        log_info("value: '" << s << "'");
    }
    void onStringVectorData(const std::vector<std::string>& vs) noexcept
    {
        for (const auto& s : vs) {
            log_info("value: '" << s << "'");
        }
    }
};

template <class T>
struct member_function_class;
template <class R, class C, class... Args>
struct member_function_class<R (C::*)(Args...)>
{
    using type = C;
};

template <class MF>
void mke_0(MF mf)
{
    static_assert(std::is_member_function_pointer<MF>::value, "Given argument is not a member function pointer");
    static_assert(std::is_base_of<test::TestListener, typename member_function_class<MF>::type>::value,
                  "Bad function pointer class");
}

void mke_1()
{
    test::TestListenerImpl impl;
    ya::app::EventDispatcher<TestListener> dispatcher;
    dispatcher.addListener(&impl);
    dispatcher.dispatchSync(&TestListener::onIntData, 66, 33);
    dispatcher.dispatchSync(&TestListener::onStringData, "jakis string");
    dispatcher.dispatchSync(&TestListener::onStringVectorData, std::vector<std::string>{"jakis string", "and again"});
}
}

int main(int argc, char* argv[])
{
    ya::utils::set_thread_name("main");
    ya::utils::set_log_level(LOG_DEBUG);

    test::mke_1();

    return 0;
}
