/*
 * Copyright (c) 2015, 2016    Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <vector>

template <int...>
struct seq
{
    seq()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
};

template <int N, int... S>
struct gens : gens<N - 1, S..., N - 1>
{
    gens()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
};

template <int... S>
struct gens<0, S...>
{
    typedef seq<S...> type;
    gens()
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
};

template <typename... Args>
int count2(Args&&... args)
{
    std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
                 "<<<<<<<<<<<<<<<<<"
              << std::endl;
    typedef typename gens<sizeof...(Args)>::type xxx_t;
    xxx_t x;
    gens<sizeof...(Args)> xx;
    std::cout << "count : " << sizeof...(Args) << std::endl;
    return 0;
}

int main()
{
    struct A
    {
    } a;
    std::vector<A> v;

    count2();
    count2(1);
    count2("asdad");
    count2(1, 2, 3, "asda");
    count2(1, 2, 3, "asda", a, v);

    return 0;
}
