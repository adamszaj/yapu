#include <gtest/gtest.h>
#include <iostream>
#include <ya/utils/flags.h>

enum Enum
{
    one,
    two,
    three,
    four
};

using flags = ya::utils::flags<Enum, 4>;

using std::cout;
TEST(TestFlags, init_1)
{
    flags fl;
    ASSERT_EQ(1, fl.none());
    ASSERT_EQ(0, fl.any());
    ASSERT_EQ(0, fl.all());
}

TEST(TestFlags, init_2)
{
    flags fl("1111");
    ASSERT_EQ(0, fl.none());
    ASSERT_EQ(1, fl.any());
    ASSERT_EQ(1, fl.all());
}

TEST(TestFlags, set_1)
{
    flags fl;
    ASSERT_EQ(0, fl.any());
    fl.set({one, three});
    ASSERT_EQ(1, fl.all_of({one, three}));
    ASSERT_EQ(0, fl.any_of({two, four}));
    fl.set({two, four});
    ASSERT_EQ(1, fl.all_of({two, four}));
    ASSERT_EQ(1, fl.all());
    ASSERT_EQ(0, fl.none());
}

TEST(TestFlags, set_2)
{
    flags fl;
    ASSERT_EQ(1, fl.none());
    fl.set();
    ASSERT_EQ(1, fl.all());
}

TEST(TestFlags, clear_1)
{
    flags fl;
    fl.set();
    ASSERT_EQ(1, fl.all());
    fl.clear({two, four});
    ASSERT_EQ(1, fl.none_of({two, four}));
    ASSERT_EQ(0, fl.any_of({two, four}));
    ASSERT_EQ(0, fl.all_of({two, four}));

    ASSERT_EQ(0, fl.none_of({one, three}));
    ASSERT_EQ(1, fl.any_of({one, three}));
    ASSERT_EQ(1, fl.all_of({one, three}));
    fl.clear({one, three});
    ASSERT_EQ(1, fl.none());
}
