#!/bin/bash

for toolchain in gcc clang ; do
    BUILD=build-${toolchain}
    rm -rf ${BUILD}
    mkdir ${BUILD}
    pushd ${BUILD}
    cmake -DCMAKE_TOOLCHAIN_FILE=cmake/${toolchain}.cmake -DCMAKE_BUILD_TYPE=Debug -DGMOCK_BUILD=ON -DYAPU_ENABLE_TESTS=ON ..
    make
    popd
done

exit

mkdir build-gmockext
pushd build-gmockext
cmake .. -DCMAKE_BUILD_TYPE=Debug -DBUILD_GMOCK=ON -DYAPU_ENABLE_TESTS=ON
make
popd
