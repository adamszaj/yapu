if (GMOCK_BUILD)

    if("${GMOCK_SRC_DIR}" STREQUAL "")
        # use default source path for gmock
        set(GMOCK_SRC_DIR /usr/src/gmock)
        message("Using default gmock source dir GMOCK_SRC_DIR: ${GMOCK_SRC_DIR}")
    endif()
    if ("${GMOCK_BUILD_DIR}" STREQUAL "")
        # set(GMOCK_BUILD_DIR ${CMAKE_BINARY_DIR}_${CMAKE_BUILD_TYPE}_GMOCK)
        set(GMOCK_BUILD_DIR ${CMAKE_BINARY_DIR}/GMOCK)
    endif()
    add_subdirectory(${GMOCK_SRC_DIR} ${GMOCK_BUILD_DIR})

    set(GMOCK_LIBRARIES gmock)
    set(GMOCK_MAIN_LIBRARIES gmock_main)
    set(GMOCK_BOTH_LIBRARIES ${GMOCK_LIBRARIES} ${GMOCK_MAIN_LIBRARIES})

    set(GTEST_LIBRARIES gtest)
    set(GTEST_MAIN_LIBRARIES gtest_main)
    set(GTEST_BOTH_LIBRARIES ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES})

elseif (GTEST_BUILD)

    if ("${GTEST_SRC_DIR}" STREQUAL "")
        # use default source path for gtest
        set(GTEST_SRC_DIR /usr/src/gtest)
        message("Using default gtest source dir GTEST_SRC_DIR: ${GTEST_SRC_DIR}")
    endif()
    if ("${GTEST_BUILD_DIR}" STREQUAL "")
        set(GTEST_BUILD_DIR ${CMAKE_BINARY_DIR}/GTEST)
    endif()
    add_subdirectory(${GTEST_SRC_DIR} ${GTEST_BUILD_DIR})
    set(GTEST_LIBRARIES gtest)
    set(GTEST_MAIN_LIBRARIES gtest_main)
    set(GTEST_BOTH_LIBRARIES ${GTEST_LIBRARIES} ${GTEST_MAIN_LIBRARIES})

else ()

    find_package(GTest REQUIRED)
    find_package(GMock REQUIRED)

    # include_directories(${GTEST_INCLUDE_DIRS})
    # include_directories(${GMOCK_INCLUDE_DIRS})
endif ()


