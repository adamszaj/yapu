function(MakePkgConfigFile arg)
    add_custom_command(
        OUTPUT ${arg}
        COMMAND cp ${CMAKE_CURRENT_SOURCE_DIR}/${arg} ${CMAKE_CURRENT_BINARY_DIR}/${arg}
        COMMAND sed -i -re "s@__PREFIX__@${CMAKE_INSTALL_PREFIX}@" -e "s@__VERSION__@${YAPU_VERSION}@" ${CMAKE_CURRENT_BINARY_DIR}/${arg})
    string(REGEX REPLACE "\\." "-" target_name ${arg})
    add_custom_target(${target_name} ALL DEPENDS ${arg})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${arg} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/pkgconfig)
endfunction()

function(MakePkgConfigFiles)
    foreach(srcpc ${ARGV})
        MakePkgConfigFile(${srcpc})
    endforeach()
endfunction()

function(CreateMakeTarget target_base source_dir tgt dpd)
    set(target ${target_base}-${tgt})
    set(target_tag ${CMAKE_CURRENT_BINARY_DIR}/.${target}.tag)

    if(NOT "${dpd}" STREQUAL "")
        set(depend ${CMAKE_CURRENT_BINARY_DIR}/.${target_base}-${dpd}.tag)
        add_custom_command(
            OUTPUT ${target_tag}
            COMMAND make -C ${source_dir} -f Makefile.ext ${tgt}
            COMMAND touch ${target_tag}
            DEPENDS ${depend}
            )
    else()
        add_custom_command(
            OUTPUT ${target_tag}
            COMMAND make -C ${source_dir} -f Makefile.ext ${tgt}
            COMMAND touch ${target_tag}
            )
    endif()
    add_custom_target(${target} DEPENDS ${target_tag})
endfunction()

function(CreateMakeTargetClean _target_base _source_dir)
    set(target ${_target_base}-clean)
    set(target_tags "")

    foreach(_tgt ${ARGN})
        set(target_tag ${CMAKE_CURRENT_BINARY_DIR}/.${_target_base}-${_tgt}.tag)
        set(target_tags ${target_tags} ${target_tag})
    endforeach()
    
    add_custom_target(${target} 
        COMMAND rm -f ${target_tags}
        )
endfunction()

function(CreateMakeTargets target_base source_dir)
    unset(dpd)
    foreach(tgt ${ARGN})
        CreateMakeTarget(${target_base} ${source_dir} ${tgt} "${dpd}") 
        set(dpd ${tgt})
    endforeach()
    CreateMakeTargetClean(${target_base} ${source_dir} ${ARGN}) 
endfunction(CreateMakeTargets)
