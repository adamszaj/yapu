# README #

## Description ##

Yet Another Program Utils - project contains set of classes and functions which I are helpful in most projects in which I took part.
You can find here Thread Multi Pool with Timer, Event Dispatcher, Command Line Parser.

This project is under active development therefore some major changes may take place before release of version 1.0.
I want to keep it small and easy to use library.

## TODO ##

Things needs to be done before version 1.0

* Add tests
* Add documentation
* Add support to asynchronous event dispatching
* Add Command Line Parser
* Change File Names to match class name inside file

## How to start ##
```

#!/bin/bash

cd yapu
mkdir build
cd build
cmake -G'Unix Makefiles' ..

```
